\section{Control theory}
\label{sec:control_theory}

In this section, we will give a brief introduction to control theory directed towards speed control of a model car.
Control theory and control engineering are huge fields, of which we will only scratch the surface. We will have a highly practical view, focusing on what knowledge is needed to design a simple speed controller for the \gls{abb:psaf} \gls{abb:ucboard}. We will start by introducing the general concept of a control loop and then narrow it in for speed control. Then, we will describe the purpose and inner workings of a feed-forward controller and a PI-controller, since these are the two standard controllers we will use when implementing a speed controller on the \gls{abb:psaf} \gls{abb:ucboard}.

\subsection{A basic control loop}
\label{sec:basic_control_loop}

In its most basic sense, a control system is a system that controls a \textit{plant}. Generally speaking, the objective is to make some output, say $y$, behave in a desired way by manipulating some input, say $u$ \cite{doyle2013feedback}. Most control systems use a sensor to measure the plant output $y$, which gives us a sensor output signal $y_\mathrm{m}$ that is fed back to the controller, as shown in \autoref{fig:basic_feedback_loop}. This is called a feedback loop. The control system as a whole takes three inputs from the outside world -- the reference or command input $y_\mathrm{r}$, the plant disturbance $d$ and the sensor noise $n$.

\begin{figure}
    \centering
    \includegraphics[width=0.65\textwidth]{figures/drawio/basic_feedback_loop.pdf}
    \caption{A basic feedback loop.}
    \label{fig:basic_feedback_loop}
\end{figure}

In control theory, there is an important distinction between process control systems, in which the goal is to make the plant output $y$ stay close to a constant reference value $y_\mathrm{r}$, and servomechanisms, in which the goal is to make the plant output $y$ follow a variable reference value $y_\mathrm{r}$. Whether the control system to design is categorized as a process control system or a servomechanism has important impacts on how the control system best can be designed \cite{balchen2016regtek}.

\subsection{Speed controller}
\label{sec:speed_controller_theory}

\autoref{fig:speed_controller} shows a simplified diagram of a speed control system. It is a simple closed control loop consisting of a motor, a speed sensor and a speed controller. The speed controller provides the motor some input $u$.
% as a continuous \gls{abb:pwm} signal. The \gls{abb:pwm} signal has given maximum and minimum values as well as dead zone.
For our purpose, we will assume that $u$ is a scalar drive value.
%  is a linear drive value from $-500$ to $1000$ for forwards drive and from $0$ to $500$ for backwards drive. Negative forwards drive values correspond to breaking. We use these ranges for drive value  We will come back to why these exact values are chosen in \autoref{ch:implementation}.
The motor moves the car and thus produces a speed $v$ as an output. The speed is measured by a speed sensor and provided as a feedback $v_{\mathrm{m}}$ to the speed controller.
% The speed controller's task is to make sure $v_{\mathrm{m}}$ is as close to $v_\mathrm{r}$ as possible.
The speed controller takes the requested speed $v_\mathrm{r}$ as an input from the outside world. The plant disturbance $d$ and the sensor noise $n$, which were depicted in \autoref{fig:basic_feedback_loop}, are ignored for simplicity. A speed controller is a servomechanism, since $v_\mathrm{r}$ is variable. For this reason, we will from now on note $v_\mathrm{r}$ as $v_\mathrm{r}(t)$.
% However, in our analysis, we will consider $v_\mathrm{r}$ piecewisely constant, rather than continuously changing, as this better reflects $v_\mathrm{r}$'s behavior in practice. This means, our equations will not use the notation $v_\mathrm{r}(t)$, even though we use $v_\mathrm{m}(t)$.

\begin{figure}
    \centering
    \includegraphics[width=0.65\textwidth]{figures/drawio/speed_controller.pdf}
    \caption{Simplified diagram of a speed control system.}
    \label{fig:speed_controller}
\end{figure}


\subsection{Feed-forward control}
\label{sec:feed_forward_control_theory}

In servomechanisms, the largest impact on the system is usually the reference value. Therefore, it is often beneficial to apply feed-forward of the reference value within the controller \cite{balchen2016regtek}.

% Several environmental parameters affect how the motor and car respond to the applied drive value, like e.g. degree of ascent, wind, weight of car with load, steering angle and last but not least, the charge status of the battery. Now, if all these parameters were constant, it is quite safe to assume that the car's response to any constant drive value is to accelerate or decelerate to an equilibrium speed and then keep it. Now, by knowing the drive value corresponding to any equilibrium speed, we can control the speed very accurately, without using any feedback. This would be a pure feed-forward controller.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{figures/plots/speed_controller_feed_forward_reference_example.pdf}
    \caption{Visualization of example feed forward control.}
    \label{fig:speed_controller_feed_forward_reference_example}
\end{figure}

In the case of a speed controller, we can calculate a feed-forward drive value $u_\mathrm{FF}(t)$ from the currently requested speed $v_\mathrm{r}(t)$ by doing a linear interpolation of $v_\mathrm{r}(t)$ against a finite set of $N$ reference pairs, each pair consisting of a speed $V_n$ and a drive value $U_n$ for $n \in \{0 \dots N\}$ like so:

\begin{equation}
    \label{eq:uff}
    \begin{aligned}
        u_\mathrm{FF}(t) & = U_{n-1} + \frac{U_{n} - U_{n-1}}{V_{n} - V_{n-1}} \cdot (v_\mathrm{r}(t) - V_{n-1}), \\
        v_\mathrm{r}(t)  & \in [V_{n-1}, V_{n}\rangle,
    \end{aligned}
\end{equation}

where $V_n$ and $U_n$ are sorted in ascending order. A visualization of this is shown in \autoref{fig:speed_controller_feed_forward_reference_example}.

% For the purpose of controlling the speed of a car by manipulating the drive value, the controller must use a wide range of motor input values to control the speed across a wide range of speed values. If we assume there is no wind and no hills (which is usually the case for our model car), the parameters that affect the  When a given motor input value is applied, the equilibrium speed of the car, the constant speed of the car after it is done accelerating as response to a change in motor controller input, is roughly the same each time. , which have mostly static relationship to the equilibrium speed (the constant speed of the car after it is done accelerating as response to a change in motor controller input).

\subsection{PI-controller}
\label{sec:pi_controller}

A PI-controller is one of the four most common forms of standard controllers. Standard controllers are beneficial for constructing control systems, since they have wide-range tunable parameters and usually give a satisfactory result \cite{balchen2016regtek}.

A PI-controller is a controller consisting of a proportional and an integral term. A purely proportional controller is a simple configurable amplifier, and is used to do linear compensation of the control error $e(t) = v_\mathrm{r}(t) - v_\mathrm{m}(t)$. The main goal of the integral term is achieve zero stationary error \cite{balchen2016regtek}. The proportional term is given by

% A proportional speed control algorithm outputs a drive value that is proportional to the difference between the requested speed and the measured speed,

\begin{equation}
    \label{eq:up}
    u_\mathrm{P}(t) = K_\mathrm{P} \cdot (v_\mathrm{r}(t) - v_\mathrm{m}(t)),
\end{equation}

and the integral term is given by

\begin{equation}
    \label{eq:ui}
    u_\mathrm{I}(t) = K_\mathrm{I} \cdot \int v_\mathrm{r}(t) - v_\mathrm{m}(t) \mathrm{d} t
\end{equation}

\cite{lichtman2017theory}. The proportional constant $K_\mathrm{P}$ and the integral constant $K_\mathrm{I}$ are tunable parameters. The resulting drive value $u_\mathrm{PI}$ is given by the sum,

\begin{equation}
    u_\mathrm{PI}(t) = u_\mathrm{P}(t) + u_\mathrm{I}(t).
\end{equation}

\subsection{Discrete-time PI-controller}

While expressions in the continuous time domain are pretty to look at, they are unfeasible to implement directly on a microcontroller. We need a way to express our controller algorithm with formulas that can be computed in discrete time steps. To do this we can apply the trapezoidal integration rule on $u_\mathrm{PI}(t)$ \cite{balchen2016regtek,lichtman2017theory}. Let $k \in \{0, \mathbb{N}\}$ denote a time step in which the speed controller algorithm calculates a new drive value, so that $v_{\mathrm{m},k}$ is the speed measured in time step $k$. The time steps occur with a constant time interval $T_\mathrm{S}$, the sampling period. Then, the drive value to apply until the next time step $k+1$ can be expressed as

\begin{equation}
    \label{eq:pi-discrete}
    \begin{aligned}
        u_{\mathrm{PI},k+1} =  K_\mathrm{P} \cdot (v_{\mathrm{r},k} - v_{\mathrm{m},k}) + K_\mathrm{I} \cdot \sum_{i \leq k} (v_{\mathrm{r},i} - v_{\mathrm{m},i}) \cdot T_\mathrm{S}.
    \end{aligned}
\end{equation}

% As we mentioned in \autoref{sec:speed_controller_theory}, $v_\mathrm{r}$ is noted as constant even though it is variable. In the above equation,

\subsection{Combined feed-forward and PI-controller}
\label{sec:ffpi_controller_background}

% Feed-forward control is mostly applicable for servomechanisms, since the reference value changes more often and therefore contributes to much impact on the plant. The feed-forward controller is there to compensate for changes in the reference value. The PI-controller, on the other hand, is sensitive to the difference between the reference value and the measured output.

In servomechanisms it is often beneficial to combine the reference-value-adaptive properties of a feed-forward controller with the disturbance resistance of a PI-controller \cite{balchen2016regtek}. This can be done by simply adding the terms together, like so:

\begin{equation}
    \label{eq:uffpi}
    u_\mathrm{FF+PI}(t) = u_\mathrm{FF}(t) + u_\mathrm{PI}(t),
\end{equation}

which becomes

\begin{equation}
    \label{eq:uffpi-discrete}
    u_{\mathrm{FF+PI},k+1} = u_{\mathrm{FF},k+1} + u_{\mathrm{PI},k+1}
\end{equation}

in the discrete time domain. The full control structure for a combined feed-forward and PI speed controller is shown in \autoref{fig:ffpi_speed_controller}.

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/drawio/ffpi_speed_controller.pdf}
    \caption{Control structure for a combined feed-forward and PI speed controller.}
    \label{fig:ffpi_speed_controller}
\end{figure}



% \subsection{PI-controller parameter tuning}
% \label{sec:pi_controller_tuning}

% \todo{Dette skal kanskje i methodology? Eller experimental setup?}

