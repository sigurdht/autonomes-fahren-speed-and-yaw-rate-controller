\section{FreeRTOS}

FreeRTOS is a free and open-source \glsxtrlong{abb:rtos} for embedded systems. It is suitable for microcontrollers thanks to its minimal memory usage and limited feature set. In this section, we will look more into detail of the features FreeRTOS provides that are relevant to us \cite{freertos2023api,barry2016freertosbook}.

\subsection{The FreeRTOS API}

The FreeRTOS \glsreset{abb:api}\gls{abb:api} is made up of a set of functions, commonly referred to as FreeRTOS' \gls{abb:api} functions. The application interfaces the FreeRTOS through calling these functions. Like common functions, the \gls{abb:api} functions can accept function arguments and return a return value. \gls{abb:api} functions are sometime referred to as ''kernel calls'' or ''system calls'', since they invoke the \gls{abb:os} kernel. \gls{abb:api} functions are used to perform operations like creating a task, deleting a task, delaying a task or interact with synchronization primitives.

In addition to the \gls{abb:api} functions, the FreeRTOS \gls{abb:api} also includes a set of port macros. To make FreeRTOS compatible with and optimized for different \glspl{abb:cpu} architectures, each supported \gls{abb:cpu} architecture has its own port. The port is made up of a C file and a header file that defines \gls{abb:cpu} architecture-specific code. For the \gls{abb:ucboard} software, for example, we will use the ARM Cortex-M4F port. Port macros are conceptually similar to \gls{abb:api} functions, but they are macros, and they are provided by the port, and not by the regular kernel.

\subsection{Tasks and scheduling}

FreeRTOS features a priority-driven preemptive scheduler. The scheduler runs each time a task ''yields'' and each time a systick \gls{abb:irq} is raised. A task can yield explicitly by calling the \verb|portYIELD| port macro, or implicitly by calling a yielding \gls{abb:api} function.

Tasks can be created with the \verb|xTaskCreate| \gls{abb:api} function, taking the task's priority as an argument. Each task has its own execution stack, storing function calls and local variables. FreeRTOS requires the maximum possible stack usage by each task to be allocated for its stack from the task is created until it is deleted. This ensures that the task always can be resumed if it is requested.

FreeRTOS task priorities cannot be interleaved with \gls{abb:isr} priorities \cite{barry2016freertosbook}, which means that each task will have a lower priority than all \glspl{abb:isr}.

FreeRTOS tasks can generally be divided into two non-overlapping categories; system tasks and application tasks. System tasks are created and managed automatically by the \gls{abb:rtos} itself, while application tasks are created and managed by the application. In FreeRTOS, there are two system tasks; the idle task, which is run when no other task is runnable, and the timer task, also called the daemon task, which is responsible for executing timer callbacks \cite{barry2016freertosbook}.

\subsection{The stack size of a task}

At a given time instant, the stack usage of a task is equal to the sum of the stack usages of the functions that are currently in the task's call stack.
Say, a task calls a function A, which calls a function B, which calls a function C, then the stack usage is the sum of the stack usages of A, B and C. But if A calls a function D after B has returned, the total stack usage is only the sum of the stack usages of A and D.
In other words, the stack usage of a task varies throughout the task['s] execution. The stack size of the task has to be greater than (or equal to) the maximum stack usage it can ever have. If the stack usage becomes greater than the allocated stack size, the task stack will use memory that is not allocated to it which is likely to cause unpredictable bugs.

The total stack usage of a task, which consists on functions calling other functions, cannot generally be known exactly at compile time, since conditional branches in the program are performed dependent on dynamic input. There exist tools to estimate or calculate an upper bound for this, like for example StackAnalyzer \cite{imicro2023stackanalyzer}.

\subsection{Synchronization primitives}

To enable safe task interaction, FreeRTOS provides synchronization primitives. The only synchronization primitive especially relevant for this report is the queue. A queue lets items be sent from one task to another. If the length of the queue is greater than $1$, multiple items can be in transmit at the same time. Any task can send items to a queue with the \verb|xQueueSend| \gls{abb:api} function, and any task can receive items from a queue with the \verb|xQueueReceive| \gls{abb:api} function. If a task tries to receive an item from an empty queue it will be blocked until an item has arrived or until a given maximum block time has run out.

\subsection{Timer callbacks}
\label{sec:timer_callbacks}

In addition to tasks, FreeRTOS provides a secondary routine scheduling mechanism called software timers. A software timer is a FreeRTOS object that is responsible for the execution of a timer callback. A timer callback is a function created by the application writer that executes the code that needs to be scheduled. Software timers can be created with the \verb|xTimerCreate| \gls{abb:api} function. A software timer can be configured to run once or to run periodically until it is stopped. When running periodically, a period must be configured.

A timer callback is a more lightweight type of routine, as it doesn't have its maximum possible stack usage allocated for itself. Instead, all timer callbacks share the same allocated stack space, and only one of them can use it at a time. But this also makes them limited, since they cannot be assigned individual priorities, and if one timer callback hangs up, it will block all other timer callbacks at the same time. Software timer scheduling is performed by a special FreeRTOS system task called the timer task, and it is the timer task's priority and maximum stack size that define the priority and maximum stack size of every timer callback.

As timer callbacks are callbacks, they enforce a stricter programming pattern than tasks. Say a routine A needs to wait for another routine B to send a value to a queue, if A is a task, A can simply do a blocking \gls{abb:api} call to \verb|xQueueReceive| and wait. If A is a timer callback, A must first check if there is something in the queue, and if it is not, it needs to to exit, store necessary state outside of its stack and check the queue again the next time it runs. It should be mentioned that since timers do run in a task, it absolutely \textit{can} do blocking \gls{abb:api} calls, but it is bad practice, since it will block all other timer callbacks at the same times.

\subsection{Non-RTOS real-time systems}

This report might up until now have given the impression that utilizing an \gls{abb:rtos} is an absolute necessity when designing a real-time system. But this is by no means a universal truth, many well established techniques for writing good embedded software without the use of an \gls{abb:rtos} exist. If the system being developed is simple, not using in \gls{abb:rtos} might very well be the most appropriate solution \cite{barry2016freertosbook,melkonian2000get}. As we will see in \autoref{sec:psaf_ads}, the present \gls{abb:psaf} \gls{abb:ucboard} software does not use an \gls{abb:rtos}, and is still capable of scheduling conceptually individual routines by periodically running callback functions sequentially. However, in this work, the goal is to investigate how we can use an \gls{abb:rtos} to enhance the present system, and in this regard, investigating non-\gls{abb:rtos} approaches is of little interest.
