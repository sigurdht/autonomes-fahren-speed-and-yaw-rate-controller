\section{The ADS of the Project Seminar Autonomes Fahren}
\label{sec:psaf_ads}

In this section we will describe the present \glsreset{abb:psaf}\gls{abb:ads} of the \gls{abb:psaf} at the \gls{abb:tudarmstadt}, including its hardware, its software and speed control mechanism. It is purely a description of the state of the \gls{abb:ads} before any contributions from our work are applied.

The \gls{abb:ads} of the \gls{abb:psaf} possesses two computer systems; the \glsreset{abb:obc}\gls{abb:obc} and the \glsreset{abb:ucboard}\gls{abb:ucboard}. While the \gls{abb:ucboard} is a single-core microcontroller with a total of \SI[]{80}{\kilobyte} of \gls{abb:ram}, the \gls{abb:obc} is a considerably more powerful Linux machine featuring 4 \gls{abb:cpu} cores and \SI[]{8}{\gigabyte} of \gls{abb:ram} \cite{mitac2023pd10bi,intel2023processor}. The \gls{abb:obc} and the \gls{abb:ucboard} communicate over a custom serial protocol through a \gls{abb:usb} cable. Within the protocol, the \gls{abb:obc} holds the role as commander, and the \gls{abb:ucboard} holds the role as worker. By commander/worker, we refer to the same type of relationship which is historically referred to as master/slave in technical literature.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/drawio/psaf_ads.pdf}
    \caption{Simplified overview of the physical components of a \glsxtrshort{abb:psaf} car and their interconnections.}
    \label{fig:psaf_ads}
\end{figure}

A simplified overview of the physical components of a \gls{abb:psaf} car and their interconnections is shown in \autoref{fig:psaf_ads}. The \gls{abb:obc} is responsible for reading and processing output from the camera, as this involves a lot of image processing, which is way too resource intensive for the \gls{abb:ucboard}. The \gls{abb:ucboard} is responsible of reading data from all other sensors; the \gls{abb:imu}, \glspl{abb:us} and the hall sensor. The \gls{abb:ucboard} also controls actuators; \glspl{abb:led}, the steering servo and the propulsion motor. The propulsion motor is operated via an \gls{abb:esc}.

There currently exist two slightly different car models used in the \gls{abb:psaf}; model 1 and model 2. Additionally, a car model 3 is currently in development. The model described above, and the model we will generally refer to if not otherwise is specified, is model 1. Model 2, however, is for the most part equal to model 1, but some aspects differ. For us, the most notable differences are:

\begin{itemize}
    \item Model 2 uses an encoder as speed sensor instead of a hall sensor.
          % \item Model 2 has an \gls{abb:rc} receiver.
    \item Model 2 uses a different type of \gls{abb:imu}.
    \item Model 2 uses a different type of \glsxtrlongpl{abb:us}.
\end{itemize}

Model 3 will be mostly similar to model 2, the only interesting difference for our part is that it will have a different motor, a \gls{abb:bldc} motor, and a dedicated motor controller board with integrated speed controller, and therefore neither a hall sensor nor an encoder as speed sensor, as the \gls{abb:bldc} motor is orientation-aware and can report the driving speed on its own.

\subsection{Serial protocol}
\label{sec:serial_protocol}

The \gls{abb:obc} and the \gls{abb:ucboard} communicate over a custom serial protocol. The serial transfer speed is configured to \SI[]{921600}{\bit/s} and uses 8-bit \gls{abb:ascii} encoding with one start bit, one stop bit, no parity bits and no hardware flow control. This gives us a data transfer speed of \SI[]{92160}{characters/s} or \SI[]{92.16}{characters/ms} \cite{ibm2023start}. The \gls{abb:obc} holds the role as commander and the \gls{abb:ucboard} as worker, as the \gls{abb:obc} sends the \gls{abb:ucboard} commands, which the \gls{abb:ucboard} executes and responds to. A complete list of all available commands with descriptions can be found in \cite{lenz2021beschreibung}.

In principle all commands are structured so that they either set a value or get/retrieve a value.
Set-commands are prefixed by an exclamation mark (\verb|!|), while get-commands are prefixed by a question mark (\verb|?|).
For example, to drive forwards with a drive value of $500$, the \gls{abb:obc} sends the following message to the \gls{abb:ucboard}:

\lstset{language=bash,style=commandlinestyle,label=lst:drv_forward_example}
\begin{lstlisting}
!DRV F 500
\end{lstlisting}

. Each command sent by the \gls{abb:obc} should have a response from the \gls{abb:ucboard}. Responses are prefixed by a colon (\verb|:|). For set-commands, the response is usually just the the value that was set or sometimes even just \verb|:ok|. For get-commands, the response contains the value that was read. For example, to retrieve the current drive mode and value, the \gls{abb:obc} sends the following message to the \gls{abb:ucboard}:

\lstset{language=bash,style=commandlinestyle,label=lst:drv_forward_example}
\begin{lstlisting}
?DRV
\end{lstlisting}

, and the \gls{abb:ucboard} responds with

\lstset{language=bash,style=commandlinestyle,label=lst:drv_forward_example}
\begin{lstlisting}
:F 500
\end{lstlisting}

. However, responses are not the only messages that the \gls{abb:ucboard} can send to the \gls{abb:obc}, it can also send messages through output streams, most notable of which being the \gls{abb:daq}['s] output stream.

\subsection{UcBoard}
\label{sec:ucboard_background}

The microcontroller on the \gls{abb:ucboard} is an STM32F303VE microcontroller. It features an ARM Cortex-M4 32-bit CPU with \SI[]{512}{\kilobyte} flash memory and \SI[]{64}{\kilobyte} \gls{abb:sram}. Additionally, it features \SI[]{16}{\kilobyte} routine boosting \gls{abb:ccram} \cite{st2016datasheet}. This means that \gls{abb:ccram} can be used by time critical routines to lower runtime, as \gls{abb:ccram} is faster than conventional \gls{abb:ram}. But the \gls{abb:ccram} also has some limitations, it is for example not accessible by the \glsreset{abb:dma}\gls{abb:dma} controller, so it can not be used for \gls{abb:dma} interaction. Other hardware features of interest include high precision hardware timers, \gls{abb:usart} controller, \gls{abb:spi} controller and \gls{abb:i2c} controller.

\subsubsection{Software}
\label{sec:present_ucboard_software}

The \gls{abb:ucboard} software is written in C without an \gls{abb:rtos}, but utilizes officially provided drivers from ST as a low-level interface to peripherals. Also without an \gls{abb:rtos}, the software is capable of executing several routines to perform all necessary functions. The routines can be categorized in the following way:

\begin{itemize}
    \item \textbf{Motor controller}    -- Responsible of low-level interactions with the \gls{abb:esc}.
    \item \textbf{Steering controller} -- Responsible of low-level interactions with the steering servo motor.
    \item \textbf{Voltage measurement} -- Measures voltage of the motor battery.
    \item \textbf{Hall sensor}         -- Calculates measured driving speed from hall sensor impulses.
    \item \textbf{IMU}                 -- Interactions with the \gls{abb:imu}.
    \item \textbf{US}                  -- Interactions with \glsxtrlongpl{abb:us}.
    \item \textbf{DAQ}                 -- Implements the \glsreset{abb:daq}\gls{abb:daq}.
    \item \textbf{CarUI}               -- User interface of car (buttons and general-purpose \glspl{abb:led}).
    \item \textbf{SYS LED}             -- Makes sure the SYS \gls{abb:led} blinks.
    \item \textbf{Communication RX}    -- Handle commands from the \gls{abb:obc}.
    \item \textbf{Communication TX}    -- Transmit messages to the \gls{abb:obc}.
\end{itemize}

Most routines are implemented as a systick callback, which is the \gls{abb:ucboard} software's abstraction of a periodically scheduled routine, without relying on an \gls{abb:rtos}. A systick callback is a function that is called by the systick \gls{abb:isr}. As explained in \autoref{sec:interrupts}, the systick \gls{abb:isr} is a periodically running function that can be used as a basis for implementing periodic routines. The concept of systick callbacks is the \gls{abb:ucboard}['s] way of of implementing periodic routines with the systick \gls{abb:isr} as a basis. The routines that are not implemented as systick callbacks are SYS \gls{abb:led}, Communication RX and Communication TX. SYS \gls{abb:led} is implemented directly in the systick \gls{abb:isr} itself, while Communication RX is implemented in the serial reception \gls{abb:isr}. Communication TX is implemented as a combination of the serial transmission \gls{abb:isr} and an infinite loop in the main thread. A major weakness of systick callbacks compared to preemptively scheduled tasks in an \gls{abb:rtos} is that all systick callbacks are called by the same function, meaning that if one systick callback consumes a lot of processing time or even hangs up completely by e.g. entering an infinite loop, the entire system is affected. With preemptive scheduling, if a low-priority task hangs up, higher priority tasks can still run without interruptions.

It should be noted that the motor controller is not the same as the speed controller. The term ''motor controller'' always refers to the routine in the \gls{abb:ucboard} software that is responsible for motor interactions. The term ''speed controller'' always refers to a software algorithm that is used to control the speed by continuously reading speed measurements and output drive values for the motor. In the present \gls{abb:psaf} \gls{abb:ads}, the speed controller algorithm runs on the the \gls{abb:obc}, while only the motor controller runs on the \gls{abb:ucboard}. In this work, we will implement a new speed controller algorithm to run on the \gls{abb:ucboard}. This speed controller will be executed by the motor controller routine, so at some point, they might seem to be the same thing, but the terms will still refer to two fundamentally different concepts.

To deal with differences between car model 1 and 2, there currently exist two different variants of the \gls{abb:ucboard} software. In particular, the software for car model 2 is a fork of the software for model 1. As described in \autoref{sec:software_evolution}, forking a software to address a different set of requirements is usually a quick and efficient solution as opposed to introducing more complex abstractions into the original source code to be able to address both sets of requirements with the same code.
% The two variants must be seen as code clones, where the variant for car model 2 is forked from the variant for model 1 \cite{mens2008identifying,kapser2008cloning}. By this, we mean that some point in time, the source code for model 2 was created as an exact copy of the variant for model 1, and then modified to meet the requirements for model 2.
Forking is especially paying off when the requirements for the two systems diverge later in the development process. However, in the case for the \gls{abb:psaf} model cars, the two different systems, model 1 and model 2, aim to serve the same purpose, but model 2 has some hardware features that model 1 does not have and vice versa. Building a model car with physical components has a significant cost, which means that the older model 1 cars are still valuable for testing and development. This implies that there is also a need to maintain the software variant for car model 1, so forking might not be an ideal solution in the long run.

\subsection{Speed control loop}
\label{sec:speed_control_loop}

% The \gls{abb:pwm} signal is sent to an \gls{abb:esc}, which controls the DC motor input voltage $u_M$. The motor gives the car a speed $v$, which is measured by a hall sensor. The hall sensor's output $\Delta t_\mathrm{hall}$


A diagram of the full speed control loop of a \gls{abb:psaf} model car is shown in \autoref{fig:speed_control_loop_old}. The speed controller is implemented on the \gls{abb:obc}. The speed controller takes two inputs, the requested speed $v_\mathrm{r}$ and the measured speed $v_\mathrm{m}$. The speed controller's output is a drive request, consisting of a driving direction and a drive value $u$. The \gls{abb:ucboard} software executes the drive request by applying a \gls{abb:pwm} signal $u_\mathrm{PWM}$ to the \gls{abb:esc}. The \gls{abb:pwm} signal has a period of \SI[]{20}{ms} and a pulse width $T_\mathrm{P}$ given by

\begin{equation}
    \begin{aligned}
        T_\mathrm{P}        & = \SI[]{1.5}{ms} + \Delta T_\mathrm{P}, \\
        \Delta T_\mathrm{P} & \in [\SI[]{-0.5}{ms}, \SI[]{+0.5}{ms}].
    \end{aligned}
\end{equation}

\begin{figure}[b]
    \centering
    \includegraphics[width=1.0\textwidth]{figures/drawio/speed_control_loop_old.pdf}
    \caption{Diagram of the PSAF speed control loop (for car model 1).}
    \label{fig:speed_control_loop_old}
\end{figure}

\begin{figure}[t]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/plots/pwm_2.pdf}
    \caption{Visualization of one period of the PWM signal when driving backwards or breaking.}
    \label{fig:pwm}
\end{figure}

A visualization of one period of the \gls{abb:pwm} signal when driving backwards or breaking is shown in \autoref{fig:pwm}. $\Delta T_\mathrm{P}$ represents the pulse width deviation from the neutral position, as $T_\mathrm{P} = \SI[]{1.5}{ms}$ corresponds to a speed of \SI[]{0}{mm/s}. A $\Delta T_\mathrm{P} < 0$ corresponds to forwards drive and a $\Delta T_\mathrm{P} > 0$ corresponds backwards drive or breaking. The \gls{abb:esc} is calibrated with a dead zone, so that any $\Delta T_\mathrm{P} \in [\Delta T_\mathrm{P,NFE}, \Delta T_\mathrm{P,NBE}]$ (neutral-forward ended and neutral-backward ended) is interpreted as neutral.

Now, the conversion from drive value $u$ to pulse width deviation $\Delta T_\mathrm{P}$ is a piecewisely linear mapping, defined differently for the two possible driving directions. For forwards drive, the mapping is from $[1, 1000]$ to $[\Delta T_\mathrm{P,NFE}, \SI[]{-0.5}{ms}]$ for positive drive values and from $[-1, -500]$ to $[\Delta T_\mathrm{P,NBE}, \SI[]{0.5}{ms}]$ for negative drive values. For backwards drive, the mapping is from $[1, 500]$ to $[\Delta T_\mathrm{P,NBE}, \SI[]{0.5}{ms}]$.

The observant eye will see that the same range of $\Delta T_\mathrm{P}$ values is used for backwards drive as well as for breaking. However, there is a significant difference between the two driving modes: When $\Delta T_\mathrm{P} > \Delta T_\mathrm{P,NBE}$, the \gls{abb:esc} will only make the motor drive backwards if backwards drive has been unlocked since the last time it was driving forwards. Backwards drive gets unlocked whenever the speed is \SI[]{0}{mm/s} while $\Delta T_\mathrm{P}$ is neutral ($\Delta T_\mathrm{P,NFE} < \Delta T_\mathrm{P} < \Delta T_\mathrm{P,NBE}$) for \SI[]{100}{ms} continuously. It is the \gls{abb:ucboard} software's task to keep track of whether backwards drive is unlocked in any situation and perform this unlocking process when necessary \cite{lenz2021beschreibung}.

The physical \gls{abb:pwm} signal $u_\mathrm{PWM}$ is generated by a hardware timer within the microcontroller. $u_\mathrm{PWM}$ propagates to the \gls{abb:esc}, which outputs the direct motor input $u_\mathrm{M}$. Now, the motor makes the car accelerate and decelerate, a process resulting in a measurable speed $v$. The speed is measured by a speed sensor. In \autoref{fig:speed_control_loop_old}, this is a hall sensor, which is the case for car model 1. As mentioned previously, in model 2, an encoder is used instead, and for model 3 the motor and entire speed control loop is about to totally change. Back on the \gls{abb:obc}, the hall sensor output $\Delta t_\mathrm{hall}$ is converted to a speed measurement $v_\mathrm{m}$ for the speed controller.

\subsection{BLDC motor controller}

In this section we will describe the interface between the  \gls{abb:ucboard} and the \gls{abb:bldc} motor controller. As previously mentioned, this is the motor that will be used on car model 3 and is neither present on car model 1 nor on model 2. The motor controller is a dedicated \gls{abb:pcb} and is connected to the \gls{abb:ucboard} over an \gls{abb:spi} bus where the \gls{abb:ucboard} is bus master. The \gls{abb:ucboard} interacts with the \gls{abb:bldc} motor controller by writing and reading registers. A full overview of available registers and their functions is described in \cite{lenz2023bldc}. An important aspect of the \gls{abb:bldc} motor controller compared to the \gls{abb:esc} used in car model 1 and 2 is that with the \gls{abb:bldc} motor controller, the exact requested driving speed in mm/s can be written to a register, and the motor controller will monitor the actual speed and keep it close to this value. With the \gls{abb:esc}, the inputted \gls{abb:pwm} does not describe the exact driving speed, but rather how much force to apply, which leaves a need to monitor and control the exact speed in an external speed control loop.
