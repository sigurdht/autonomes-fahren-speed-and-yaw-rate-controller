\section{Real-time systems}
\label{sec:real-time-systems}

A real-time system is a computer system with real-time constraints that define how quickly the system has to respond to a given set of inputs. In \cite{laplante2004real}, fundamental real-time system concepts are defined and discussed at a basic level. At the most basic level, a real-time system is defined in the following way:

\begin{definition}[Real-time system]
    A real-time system is a system that must satisfy explicit (bounded) response-time constraints or risk severe consequences, including failure.
\end{definition}

In \cite{laplante2004real}, there is further discussed how one can argue that every practical system is a real-time system, since every practical systems will be completely useless if it cannot produce outputs within a reasonable time frame. It is therefore necessary to distinguish between hard, firm and soft real-time systems.

\begin{definition}[Hard real-time system]
    A hard real-time system is one in which failure to meet a single deadline may lead to complete and catastrophic system failure.
\end{definition}

\begin{definition}[Firm real-time system]
    A firm real-time system is one in which a few missed deadlines will not lead to total failure, but missing more than a few may lead to complete and catastrophic system failure.
\end{definition}

\begin{definition}[Soft real-time system]
    A soft real-time system is one in which performance is degraded but not destroyed by failure to meet response-time constraints.
\end{definition}

\subsection{Real-time constraints}

The real-time constraints of a real-time system arise from the physical environment in which the system operates. For example, when the driver of a car presses the break pedal, the break controller must actuate the breaks within a few milliseconds to avoid a potential crash. This is a hard constraint, since missing a single deadline can have catastrophic consequences. When new speed measurement data is available, this data must be processed and the speedometer must be updated within a few tenths of a seconds, or the driver will be misinformed about how fast she/he is driving. This can lead to the driver doing bad decisions, but since the driver has an intuitive feel of how fast the car is going she/he isn't dependent on always getting instant speed data to drive safely. Therefore, this should be categorized as a firm real-time constraint. Soft real-time constraints in a car include most convenience features, such as the responsiveness of the navigation system or the air conditioner. \cite{laplante2004real} also notes that there is big room for the interpretation of hard, firm and soft real-time systems -- every system can probably be categorized any way -- soft, firm or hard -- real-time by the construction of a proper supporting scenario.

\subsection{Scheduling concepts}
\label{sec:scheduling_concepts}

A ''task'' is an abstraction of a running program \cite{laplante2004real}. It can be seen as a set of instructions that need to be executed in order to achieve some functionality. Real-time systems usually have multiple tasks, and a scheduler's responsibility is to ''schedule'' the tasks, or in other words, decide which task to execute at any time. In a single-processor system only one task can execute at a time. A task is requested or initiated at some point in time and executes until it is completed. In a real-time system each task is associated with a deadline, a point in time before which the task has to be completed. The same task is often requested multiple times.

\begin{definition}[Task]
    A task is an abstraction of a running program. A task can be preempted an resumed by the scheduler.
\end{definition}

One of the most influential works in real-time scheduling theory is a paper by Liu and Layland published in 1973 \cite{liu1973scheduling,sha2004real}. In this paper, there is described a specific, yet common, scheduling problem consisting of a set of periodically requested tasks, with deadlines defined to be the time of the next request for the same task. All tasks are assumed to be independent and preemptible and have fixed computation times. That a task is preemptible means that the scheduler can pause its execution at any time instant to run another task for a while and resume the first task again later without modifying the task's behavior. Another assumption being made is that a task cannot itself ask the scheduler to be paused, it has to be preempted.

Based on these assumptions, Liu and Layland \cite{liu1973scheduling} present the rate-monotonic priority assignment rule and proves that it is the optimal way to schedule a set of fixed-priority tasks, given these assumptions. In the same paper, they also present a deadline driven scheduling algorithm. In this algorithm, the priorities are assigned based on which task has the closest deadline. Up through the years several researchers and developers have designed and implemented various additions and modifications to these algorithms to achieve different goals.

For general-purpose computing (in contrast to real-time computing) round-robin scheduling is often a beneficial approach. In a round-robin scheduler, each task or process is fairly assigned time slices that give all tasks an equal share of the \gls{abb:cpu} time \cite{silberschatz2010process}. For general purpose computers there is often a goal that all tasks get the chance to run the same amount of time, even when the system is overloaded, this is what round-robin scheduling achieves.

% To clearify the discussion, we will from now on refer to a task that is requested periodically at a fixed rate, is independent of other tasks

The term ''task'' is very closely related to how the scheduler works. A set of tasks is a set of individually schedulable programs by the scheduler in question. However, in practical applications, we are often more interested in the function the task performs. And since most functions can be performed by multiple tasks and multiple functions can be performed by the same task, we will use the term ''routine'' when we refer to any sort of program, regardless of implementation, that performs a specific function.

\begin{definition}[Routine]
    A routine is any program that executes from it is requested until it is completed to perform a specific function.
\end{definition}

\subsection{Task interaction}

% In many situations it is useful to design tasks that depend on the execution of other tasks which often means that it has to wait for the other task to reach some point in its execution. 

% It is often useful to synchronize tasks

In most common applications some task interaction is necessary, and the exact assumptions by Liu and Layland do not hold \cite{laplante2004real}. Typical task interaction has two main objectives, to transfer data (communication) and to ensure relative timing of operations (synchronization). Proper task interaction is achieved by using synchronization primitives, such as semaphores, mutexes, events, mailboxes and so on. If task interaction is not done properly it can lead to several faults, including data corruption, infinite loops and deadlocks, to name a few.

Synchronization and communication is often two sides of the same coin. E.g. when a task A tries to fetch some data from a task B, it requires B to reach that point of execution where data is actually provided to A. If B has still not provided data when A requests it, A should wait until the data is ready. In this situation, A is blocked from executing further. When blocked, the scheduler should not execute the task it is ready. Therefore, the scheduler assigns each task one of the 4 following states \cite{laplante2004real}:

\begin{enumerate}
    \item Executing -- The task is currently executing
    \item Ready     -- The task is currently not executing, but is ready to do it
    \item Blocked   -- The task is blocked from executing since it has to wait for other tasks
    \item Dormant   -- The task is either not requested yet or is already completed
\end{enumerate}

\subsection{Real-time operating systems}
\label{sec:rtoses}

An \glsreset{abb:os}\gls{abb:os} is a layer of software that interacts directly with the system hardware and provides a developer-friendly interface for higher-level applications in the system. A \glsreset{abb:rtos}\gls{abb:rtos} is an \gls{abb:os} that is designed to support real-time system needs. An \gls{abb:rtos} usually gives more control of scheduling than a general-purpose \gls{abb:os}, and provides utilities for synchronizing threads. Many \glspl{abb:rtos} target embedded systems, and therefore try to use as little memory and \gls{abb:cpu} resources as possible. These can also be categorized as embedded \glspl{abb:os}, but that does not mean all embedded \glspl{abb:os} are \glspl{abb:rtos} \cite{holt2014embedded-1}.

FreeRTOS is an example of an embedded \gls{abb:rtos}. FreeRTOS has a minimal \gls{abb:ram} usage of $\SI{236}{\byte}$ and minimum \gls{abb:rom} usage of 5-10 $\kilobyte$ \cite{freertos2023faq}. On the other hand, RTLinux is an example of an \gls{abb:rtos} that is not meant for tiny devices. The footprint of RTLinux will even in the most stripped-down configurations be in the megabyte-range for both \gls{abb:rom} and \gls{abb:ram} usage \cite{andersson2005comparison}.

Different \glspl{abb:rtos} also have varying support for different scheduling strategies. FreeRTOS only supports one scheduling strategy, which is a fixed priority scheduling algorithm \cite{sha2004real,freertos2023scheduling} similar to the rate-monotonic priority assignment rule presented by Liu and Layland in 1973 \cite{liu1973scheduling}. The FreeRTOS scheduler can however be configured to not use task preemption and uses by default round-robin time slicing to share \gls{abb:cpu} time between tasks of the same priority \cite{barry2016freertosbook}. RTLinux supports several more sophisticated schedulers, like "earliest deadline first" \cite{andersson2005comparison}. Partly due to this, RTLinux has the potential of consuming considerably more \gls{abb:cpu} time than FreeRTOS.

\subsection{Interrupts}
\label{sec:interrupts}

Interrupts is a hardware feature present in most modern computers, including microcontrollers. It lets peripheral devices notify the \gls{abb:cpu} that some event has occurred by sending an \gls{abb:irq}. The \gls{abb:cpu} will then interrupt its current execution and start executing the \gls{abb:irq}['s] configured \gls{abb:isr}. Multiple \glspl{abb:irq} might occur at the same time, or a new interrupt request might occur while another \gls{abb:isr} is still executing. For this reason, \glspl{abb:isr} can usually be assigned priorities, just like tasks within an \gls{abb:rtos}. However, since \gls{abb:isr} execution is controlled by the hardware and not the \gls{abb:rtos}, the priorities of \glspl{abb:isr} can usually not be interleaved with priorities of tasks within an \gls{abb:rtos}. The interrupts and the \gls{abb:rtos} tasks constitute two separate priority spaces, where the \glspl{abb:isr}['] priority space is above the \gls{abb:rtos} tasks' \cite{barry2016freertosbook,leyva2006predictable}.

The systick interrupt is a special interrupt that is available on most microcontrollers. The systick interrupt occurs periodically with a configurable tick period $T_t$, configured to \SI[]{1}{ms} for the \gls{abb:ucboard} in the \gls{abb:ads} of the \gls{abb:psaf}. This makes the systick \gls{abb:isr} a function that is ensured by the hardware itself to run periodically with period $T_t$, which can be used as a basis for implementing periodic routines.
