\section{Deadlines}
\label{sec:deadlines}

In this section we will derive a set of real-time constraints, resulting in two deadlines, for the \gls{abb:ucboard} software on the \gls{abb:psaf} model car. These deadlines are not derived from a formal specification of the car or the \gls{abb:ucboard}, but rather from common sense. The purpose of this section is not to derive any formally required real-time deadlines for the \gls{abb:ucboard}, but rather to establish a meaningful real-time context in which the \gls{abb:ucboard} can be seen and evaluated.

\subsection{DRV reaction time}
\label{sec:drv_reaction_time}

The first deadline we derive is how quickly the \gls{abb:ucboard} has to change the motor's \gls{abb:pwm} signal after receiving a \verb|DRV| command. The faster the car is driving, the more crucial it is to react quickly to avoid potential crashes.

We can define a constraint that the \gls{abb:ucboard} should never let the car drive longer than a reaction distance $s_\mathrm{D}$, where D is short for ''deadline'', after a \verb|DRV| command is received before the motor's \gls{abb:pwm} signal is updated accordingly. This gives a reaction time $r = \frac{s_\mathrm{D}}{v}$, where $v$ is the current speed of the car. Since the speed is constantly changing, we must require the maximum reaction time, or deadline, to be $r_\mathrm{D} = \frac{s_\mathrm{D}}{v_\mathrm{top}}$, where $v_\mathrm{top}$ is the top speed of the car, to make sure the requirement is met for every possible speed. For example, if we require $s_\mathrm{D} = \SI[]{100}{mm}$, and assume $v_\mathrm{top} = \SI[]{2500}{mm/s}$, then we get $r_\mathrm{D} = \SI[]{40}{ms}$.

If the \gls{abb:ucboard} is not able to meet this deadline, it can in the worst case can make the model car crash into an object it otherwise would not crash into. As the entire purpose of the \gls{abb:ads} is to navigate the car through a track without crashing, we can consider a crash a complete system failure. Due to this, if the \gls{abb:ucboard} fails to meet a single DRV reaction time deadline, it might render the \gls{abb:ucboard} completely useless, meaning that the DRV reaction time is a hard real-time constraint.

\subsection{Response time}

Partly because it important for the \gls{abb:obc} to get timely responses to know what is going on and partly because it is a lot more practical to measure, we will to a large extent use the response time $R$ as an estimator for the reaction time $r$ of the respective command. The response time of a command is the time it takes from the command is received by the \gls{abb:ucboard} until the response is returned. This metric is usable for all commands since all commands should always return a response. Depending on the case, this may or may not be a good estimator. For the cases we will study, the causes for differences in the reaction time is not the command processing and execution itself, but rather other tasks that occupy \gls{abb:cpu} time. Dependent on how the command is implemented, the requested action is sometimes guaranteed to be completed before the response is sent. For such commands, the response time is an upper bound for the reaction time ($R > r$).

As a stand-alone real-time requirement, the DRV response time is rather soft, as the only information being replied back after a DRV command is a confirmation that the order was received. This is of course dependent on how the \gls{abb:obc}['s] algorithm is implemented in regard to dealing with late responses from the \gls{abb:ucboard}. But the response time for another command could be a rather hard real-time constraint, if the response contains vital information the \gls{abb:obc} actually needs for navigation in real-time.

% \subsection{Speed controller}

% When the speed controller is active, it will be a task that gets requested repeatedly at a constant rate $\frac{1}{T_S}$ and should always finish before its next request. This is the same type of deadline that Liu and Layland, which we addressed in \autoref{sec:scheduling_concepts}. This deadline is strict enough to make sure speed controller requests are not backing up, but rather run at a steady rate. As long as $T_S$ is relatively small, say less than $\SI[]{100}{ms}$, the speed controller algorithm will still run multiple times per second.
