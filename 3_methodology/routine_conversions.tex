\section{Routine conversions}
\label{sec:routine_conversions}

In this section, we will describe two methods for converting routines between different routine types.
The first method converts a systick callback, a function called directly by the systick \gls{abb:isr}, to a FreeRTOS timer callback. The primary benefit of this conversion is that it brings the routine out of the \gls{abb:isr} priority space and into FreeRTOS' priority space, which allows its priority to be superseded by high priority tasks. However, it does not give the routine free priority assignment, since all timer callbacks have the same priority. If we want the routine to get the same individual priority assignment capabilities, as a task, we can use the second method described here to further convert it to a task.

\subsection{Convert a systick callback to a FreeRTOS timer callback}

As we saw in \autoref{sec:present_ucboard_software}, in the present \gls{abb:ucboard} software, most routines are implemented as systick callbacks. In principle, we can describe the exact routine of a systick callback by its callback function $C$ and its callback period $T$. The same is true for FreeRTOS timer callbacks. This means that we can implement the same routine by executing $C$ with a period $T$ with a FreeRTOS software timer instead of calling it manually in the systick \gls{abb:isr}. The routine will now execute with the priority of the timer task. If individual priority assignment is needed, it can be further converted to a task with the method described in \autoref{sec:porting_timers_to_tasks}.

\subsection{Convert a FreeRTOS timer to a FreeRTOS task}
\label{sec:porting_timers_to_tasks}

We will now present a generic method to port a FreeRTOS timer to a FreeRTOS task. This can be useful if a routine that is currently implemented as a timer callback is considered important and should be given a higher (or lower) priority than the other timer callbacks. While FreeRTOS timer callbacks actually can have a variable request period, this approach is limited to fixed-period callbacks only.

Given we have a FreeRTOS software timer executing the callback function $C$ at a fixed period $T$, we can port the timer to  execute $C$ in its own task as shown in \autoref{alg:task_for_timer_callback}, where TaskForTimer is executed as a FreeRTOS task. This is the task version of our timer. In this way, $C$ is executed with the priority of this task, instead of the priority of the timer task. If TaskForTimer is given a higher priority than the timer task, the scheduler will, if needed, preempt the timer task for the benefit of $C$ whenever it is time for $C$ to execute. The downside is of course that $C$ is now executed in a dedicated task which requires a dedicated execution stack.

\begin{algorithm}
    \caption{A task dedicated to execute a timer callback $C$ with a fixed interval $T$.}
    \label{alg:task_for_timer_callback}
    \small
    \begin{algorithmic}
        \Procedure{TaskForTimer}{$C$, $T$}{}
            \Loop
                \State Delay $T$
                \State $C$()
            \EndLoop
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

The proposed method is a very simple approach that does not require any adaptions to the implementation of the callback function $C$. However, in many cases, a more tailored approach would be beneficial, e.g. if $C$ only needs to do some processing when an item can be received from a queue, the delay could be replaced by a blocking call to \verb|xQueueReceive| and the code that should only run after an item is received from the queue can run directly afterwards. This would also have the benefit of making the queue reception event driven, rather than the previous polling-driven approach, which means that an item from a queue can be received and further processed immediately and no processing time is wasted on polling when there is nothing there.

% There can be many situations where a timer callback (or originally a systick callback) is used to check if some even has occurred and conditionally fetch some value and do some processing. Generally speaking, we call these polling-driven approaches to event handling. FreeRTOS has
