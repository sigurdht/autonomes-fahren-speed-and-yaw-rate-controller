\section{Routine implementation}
\label{sec:routine_configuration}

We will utilize the methods from \autoref{sec:routine_conversions} to convert the routines of the \gls{abb:ucboard} software from their present non-\gls{abb:rtos} implementation to a suitable \gls{abb:rtos}-based counterpart. The routines in question are those that were described in \autoref{sec:present_ucboard_software}. We implement two slightly different routine configurations to evaluate against the present non-\gls{abb:rtos} \gls{abb:ucboard} software. The first, configuration A, using less memory than the other, configuration B, which should have more robust reaction and response times. The only point in which they differ is whether the communications routines are implemented as routines or tasks. We want to evaluate these two configurations to see if the choice of implementing a routine as a timer callback or task actually has implications for it's ability to run, which we expect it to.

% We will evaluate two different configurations of routine implementations, the A configuration and the B configuration. They differ in whether the communication routines are implemented as timer callbacks or tasks. In the A configuration the communication routines are timer callbacks, while in the B configuration they are tasks. The A configuration is called so because it uses less memory, and the B configurations is called so because it should enable the \gls{abb:ucboard} to respond to commands faster in certain situations.

\subsection{Configuration A}

Routine configuration A uses only one application task in addition to the two system tasks. The only routine being implemented as a task in configuration A is the motor controller. The remaining 10 routines are implemented as software timers. The configuration is shown in \autoref{tab:routines_minimum_memory}. The routines are sorted by decreasing priority. The system tasks are not listed, since they are not routines as seen from the application's perspective. For the timer callbacks, the priority of the timer task is indicated as the timer callback's priority. \autoref{fig:tasks_minimum_memory} shows all FreeRTOS tasks in the system and all timer callbacks as functions called by the timer task. Since most routines, including the communication routines, are implemented as timer callbacks, we should expect similar response times to the present non-\gls{abb:rtos} solution, as the communication routines are on the critical path of the response time of all commands.

The reasoning for why specifically the motor controller is implemented as a task is explained in \autoref{sec:motor_controller_implementation}. In the microcontroller software for the Warsaw University's Carolo-Cup 2018 competition car, motor control is not implemented as a dedicated task, but instead steering is. In our case, the steering controller is directly ported from a systick callback in the current non-\gls{abb:rtos} \gls{abb:ucboard} software, which means that it would require more rework to utilize the FreeRTOS \gls{abb:api}. However, porting it to a dedicated, as explained in \autoref{sec:routine_conversions}, would let us assign the steering controller a higher priority than the other timer callbacks, giving it a better chance of running when the system is overloaded.

\begin{table}
    \caption{Routine configuration A.}
    \label{tab:routines_minimum_memory}
    \centering\begin{tabular}{lll}
        \hline
        \textbf{Routine}    & \textbf{Type of implementation} & \textbf{Priority} \\ \hline
        Motor controller    & Task                            & 4                 \\
        Steering controller & Timer callback                  & 2                 \\
        Voltage measurement & Timer callback                  & 2                 \\
        Hall sensor         & Timer callback                  & 2                 \\
        IMU                 & Timer callback                  & 2                 \\
        US                  & Timer callback                  & 2                 \\
        DAQ                 & Timer callback                  & 2                 \\
        CarUI               & Timer callback                  & 2                 \\
        SYS LED             & Timer callback                  & 2                 \\
        Communication RX    & Timer callback                  & 2                 \\
        Communication TX    & Timer callback                  & 2                 \\ \hline
    \end{tabular}
\end{table}

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{figures/drawio/tasks_a.pdf}
    \caption{FreeRTOS tasks and timer callbacks in configuration A.}
    \label{fig:tasks_minimum_memory}
\end{figure}

\subsection{Configuration B}

Routine configuration B uses three application tasks in addition to the two system tasks. The routines implemented as tasks in configuration A are the motor controller, communication RX and communication TX. The remaining eight routines are implemented as software timers. The configuration is shown in \autoref{tab:routines_maximum_responsive}. Like in \autoref{tab:routines_minimum_memory}, routines are sorted by decreasing priority and system tasks are not listed. For the timer callbacks' priority, the priority of the timer task is listed. The communication tasks are given high priorities to increase the robustness of commands' response and reaction times, since they are both on the critical of the response time of all commands. Communication RX is also on the critical path of the reaction time of all commands, and has therefore a priority of 9, while communication TX only has 8. \autoref{fig:tasks_maximum_responsive} shows all FreeRTOS tasks in the system and all timer callbacks as functions called by the timer task.

\begin{table}[]
    \caption{Routine configuration B.}
    \label{tab:routines_maximum_responsive}
    \centering\begin{tabular}{lll}
        \hline
        \textbf{Routine}    & \textbf{Type of implementation} & \textbf{Priority} \\ \hline
        Communication RX    & Task                            & 9                 \\
        Communication TX    & Task                            & 8                 \\
        Motor controller    & Task                            & 4                 \\
        Steering controller & Timer callback                  & 2                 \\
        Voltage measurement & Timer callback                  & 2                 \\
        Hall sensor         & Timer callback                  & 2                 \\
        IMU                 & Timer callback                  & 2                 \\
        US                  & Timer callback                  & 2                 \\
        DAQ                 & Timer callback                  & 2                 \\
        CarUI               & Timer callback                  & 2                 \\
        SYS LED             & Timer callback                  & 2                 \\ \hline
    \end{tabular}
\end{table}

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{figures/drawio/tasks_b.pdf}
    \caption{FreeRTOS tasks and timer callbacks in configuration B.}
    \label{fig:tasks_maximum_responsive}
\end{figure}

\subsection{Task stack sizes}
\label{sec:stack_sizes_implementation}

To get a rough idea of how large stack a task needs, we use the script \hyperlink{file:analyze_stack_usage_py}{analyze\_stack\_usage.py}, which prints an overview of the functions with the highest stack usage. The script simply extracts the stack usages of functions as reported by the compiler and does not take into account the stack usage of other functions called by the function in question. Then we apply our knowledge about which functions are used by which tasks to select the stack size to allocate for each FreeRTOS task. This is not an ideal solution, finding an exact upper bound using a more sophisticated tool like StackAnalyzer would have been more beneficial, but the time did not allow for that in this work.
