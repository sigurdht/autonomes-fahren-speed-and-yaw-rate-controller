\section{Response time measurements}
\label{sec:response_time_measurements}

As a metric for the robustness of the \gls{abb:ucboard} software, we want to measure the response time $R$ of the DRV command when the \gls{abb:ucboard} is under heavy load. This means we need a reliable way to apply a computational load that is heavy enough to significantly affect the response time of the DRV command. By abusing
% This can be achieved by two fundamentally different strategies. First and foremost, we can abuse 
the existing functionality of the \gls{abb:ucboard} through e.g. activating a lot of background tasks and at the same time spam it with requests, the system might get overloaded, and we can see a drop in response time for certain commands. Unfortunately, the present \gls{abb:ucboard} software is well designed and we were not able to overload it enough to make a significant impact. Therefore, we will instead implement an artificial heavy operation in the \gls{abb:ucboard}'s software on purpose to consume computational resources.
% Unfortunately, but also thankfully, the \gls{abb:ucboard}'s firmware is well designed and we were not able to overload it by using its standard functionality. Strategy 2 is the only possible solution.

\subsection{Artificial heavy operation}
\label{sec:artificial_heavy_operation}

To overload the \gls{abb:ucboard} software, we will use \gls{abb:led} A. In detail, each time \gls{abb:led} A is powered on, an artificial heavy operation is performed. The operation consists of writing the integer value $42$ to a variable \verb|foo| a large number of times $N$. The operation is performed by the CarUI routine, and not the Communication RX routine, since we would like the operation to run in the background after the response for powering the \gls{abb:led} on is given. $N = \SI[]{2000000}{}$ was found to be a reasonable value, making the operation take approximately \SI[]{500}{ms}.

The implementation code for the heavy operation is shown in \autoref{lst:heavy_operation}. The code is to be added to the beginning of \verb|carui_do_systick_10ms| (\hyperlink{file:carui.c}{carui.c}). The variable \verb|s_bDelayDone| ensures that the operation only happens once each time the \gls{abb:led} is powered on.

\lstinputlisting[language=c,style=pythonstyle,label=lst:heavy_operation,caption={Artificial heavy operation to run when \gls{abb:led} A is powered on.}]{source_code/implementation/heavy_operation.c}

\subsection{Automated measurement script}

A Python script \verb|drv_response_time| (\hyperlink{file:automate.py}{automate.py}) is implemented to automate the experimental measurement process. It performs the following procedure 50 times:

\begin{enumerate}
    \item Select a random delay value $d \in [0.1, 1]$.
    \item Open a serial connection to the \gls{abb:ucboard}.
    \item Reset \gls{abb:ucboard} software.
    \item Drive forwards with a drivevalue of \SI[]{300}{}.
    \item Wait \SI[]{2}{s}.
    \item Power on \gls{abb:led} A.
    \item Wait $d$.
    \item Brake.
    \item Look through the serial response file and find the measured response time $R_\mathrm{m}$ for the brake command.
    \item Append $d$ and $R_\mathrm{m}$ to a CSV file.
\end{enumerate}

The \gls{abb:ucboard} should return a response to every command. To make sure the \gls{abb:ucboard} is ready, \verb|drv_response_time| always waits for the response before going further. This means that the delay $d$ is defined from the response for powering on \gls{abb:led} A is received until the command to brake is sent.
% Another possibility is to use the static time from the command for powering on \gls{abb:led} A is sent until the command to brake is sent, $d_\mathrm{s}$.
% \todo{Skal jeg si noe mer her egentlig?}
% We will see later that using the dynamic delay $d$ gives us a very interesting pattern in the $R$ values, while using the static delay $d_\mathrm{s}$ does not. An equivalent measurement process using $d_\mathrm{s}$ is implemented by \verb|drv_response_time_static| (\hyperlink{file:automate.py}{automate.py}).

\subsection{Applicability of response time as estimator for reaction time}
\label{sec:response_time_as_estimator_for_reaction_time}

How applicable the response time $R$ is as an estimator for the reaction time $r$ for the DRV command turns out to be dependent on the routine configuration. We will now explain why.

For the DRV command, actuator input is applied by the motor controller routine and not directly by the communication RX routine. But in routine configuration A and B, the motor controller is a dedicated task which uses a blocking \gls{abb:api} call to fetch drive requests from the drive request queue. In configuration A, the motor controller also has higher priority than the communication RX routine, which means that when communication RX pushes a request to the drive request queue, it will immediately be preempted by the scheduler until the motor controller is done processing it. In routine configuration A, the response time $R$ is therefore an upper bound for the reaction time $r$. For routine configuration B, however both communication RX and TX have higher priority than the motor controller, which means that the response \textit{can} be sent before actuator input is applied. For the current \gls{abb:ucboard} software it is a different story, but since the motor controller only polls for new requests once every \SI[]{1}{ms} and the communication TX polls continuously, neither here is $R$ an upper bound for $r$.

However, in all cases the heavy operation runs in the CarUI routine, and the CarUI routine is in none of the cases able to block the motor controller from running without also blocking communication TX from running. This means that as long as the heavy operation in the CarUI is the most significant contributor to system overload, the response time $R$ is still a decent estimator for the reaction time $r$.
