\section{Speed controller tuning}
\label{sec:speed_controller_tuning}

In this section, we will describe how we tune the application specific parameters of the speed controller, $U_n, V_n \; \forall \; n \in \{0 \dots N\}$, $T_\mathrm{S}$, $K_\mathrm{P}$ and $K_\mathrm{I}$. We will firstly undertake the feed-forward reference values $U_n, V_n \; \forall \; n \in \{0 \dots N\}$ in \autoref{sec:speed_controller_ff_values}, and then the PI-controller parameters $T_\mathrm{S}$, $K_\mathrm{P}$ and $K_\mathrm{I}$ in \autoref{sec:speed_controller_pi_parameter_tuning}.

\subsection{Tune feed-forward reference values}
\label{sec:speed_controller_ff_values}

Good feed-forward reference reference values, $U_n, V_n \; \forall \; n \in \{0 \dots N\}$, can quickly be found experimentally for small values of $N$ by conducting a practical experiment: We rig up a model car with the \gls{abb:ucboard}, place it on the floor and connect to the serial interface over bluetooth from a \gls{abb:pc}. Then we let the car drive around in a circle with different drive values and record the measured speed. In particular, we apply $N$ different drive values $U_n \; \forall \; n \in \{1 \dots N\}$ and measure their equilibrium speeds. The script \verb|find_feed_forward_values| (\hyperlink{file:automate.py}{automate.py}) is implemented to automate this process on the \gls{abb:pc}. This is done by repeating the following steps for all $n \in \{1 \dots N\}$:

\begin{enumerate}
    \item Setup the \gls{abb:daq} to log hall sensor time deltas over the serial protocol.
    \item Apply $U_n$ as the car's drivevalue.
    \item Wait \SI[]{15}{s}.
    \item Stop \gls{abb:daq} logging and brake.
    \item Calculate the average speed $V_n$ the last \SI[]{5}{s}.
    \item Append $U_n$ and $V_n$ to a CSV file.
\end{enumerate}

As for the selection of values for $N$ and $U_n \; \forall \; n \in \{1 \dots N\}$, we will use all drivevalues $U_n \geq 200$ that are divisible by $100$, for both driving directions. As the maximum drive value is $1000$ for forwards drive and $500$ for backwards drive, this gives us a total of $N = 13$ repetitions of the steps above. We expect the distribution of $U_n, V_n$ pairs to be mostly linear, which means that $N = 4$, giving us two pairs in each driving direction would in theory be satisfactory. $N = 13$, however, gives us a better margin to cope with minor non-linearity. Additionally, the drivevalue $0$ is for all purposes expected to have an equilibrium speed of \SI[]{0}{mm/s}, so this is also added to the results, without going through the above mentioned steps. This gives us in total $N = 14$ drive value/equilibrium speed pairs for the speed controller. In addition to producing a CSV file, \verb|find_feed_forward_reference_values| also generates \verb|.h| file containing C macros for the \gls{abb:ucboard} software config file. This simplifies the process of updating the feed forward reference values in the \gls{abb:ucboard} software in the future.

\subsection{Tune PI-controller parameters}
\label{sec:speed_controller_pi_parameter_tuning}

\cite{balchen2016regtek} gives a profound introduction to theory and systematic rules for PI-controller tuning. However, we will not apply any of this in this work, as the main motivation for the speed controller is that the \gls{abb:obc}['s] interface to car model 1 and 2 achieves compatibility with the next generation of \gls{abb:psaf} model cars. A solution providing ''good enough'' performance is perfectly satisfactory. For this reason, we will take a simple try-and-observe approach, by observing the impulse response of the speed controller for a limited set of parameter values.

In a similar fashion as for finding feed forward reference values, we measure the final impulse response of the speed controller by driving the car and measure speed through the \gls{abb:daq} over the serial protocol. The procedure is as follows for a target speed $v_\mathrm{r}$:

\begin{enumerate}
    \item Setup the \gls{abb:daq} to log hall sensor time deltas over the serial protocol.
    \item Apply $v_\mathrm{r}$ as the speed controller's target speed.
    \item Wait \SI[]{10}{s}.
    \item Stop \gls{abb:daq} logging and brake.
    \item Write measured speed values with timestamps to a file.
\end{enumerate}

The measured impulse response is specific to given values of the parameters $T_\mathrm{S}$, $K_\mathrm{P}$ and $K_\mathrm{I}$. We ended up measuring the impulse response for $56$ different combinations of $K_\mathrm{P}$ and $K_\mathrm{I}$, which are all $K_\mathrm{P} \in \{0, 50, 100, 200, 500, 1000, 2000\} \; \mathrm{(m/s)^{-1}}$ and all $K_\mathrm{I} \in \{0, 5, 10, 20, 50, 100, 200, 500\} \; \mathrm{m^{-1}}$. To limit the extensiveness of our speed controller evaluation, we did not test multiple values of $T_\mathrm{S}$, and went with \SI[]{50}{ms}. This is a reasonable value, since the \gls{abb:esc} is controlled by a \gls{abb:pwm} signal with a period of \SI[]{20}{ms}, and any value lower than this would make the speed controller update the drive value more often than the \gls{abb:esc} can sense a drive value change. By selecting \SI[]{50}{ms} our $T_\mathrm{S}$ is above this with a good margin. Having a too high $T_\mathrm{S}$ is undesired due to the sampling theorem \cite{balchen2016regtek}.

For each combination of parameters, we measure the impulse response for three different target speeds $v_\mathrm{r}$ to verify how the parameters work for a greater range of target speeds. We use the three target speeds \SI[]{0.5}{m/s}, \SI[]{1}{m/s} and \SI[]{1.5}{m/s}. These are selected since they represent the speed controller's typical operating range well.

As a scalar metric of how responsive the speed controller is with a given set of parameters, we will use the average acceleration time $\overline{T_\mathrm{a}}$ for the three selected target speeds. By acceleration time $T_\mathrm{a}$, we mean the time passed from the request is given until the car reaches the requested speed for the first time. The average acceleration time is the arithmetic mean of the acceleration times,

\begin{equation}
    \overline{T_\mathrm{a}} = \frac{1}{N} \sum_{i=0}^{N-1}T_{\mathrm{a},i},
\end{equation}

where $N$ is the number of target speeds tested, $3$ in our case, and $T_{\mathrm{a},i}$ is the acceleration time for target speed $i$. A low value for $\overline{T_\mathrm{a}}$ implies that the speed controller responds quickly to changes in the target speed, which is good.
This is
% unfortunately
not a perfect way to summarize the speed controllers, since a speed controller that undershoots can get an artificially high value, even infinitely high if it never reaches the exact target speed at all. But it is very easy to understand.
