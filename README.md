# Sigurd Hagen Tullander's master thesis report

You can find the report [here](./main.pdf).

## Build

Render drawio diagrams

```sh
drawio --crop -xf pdf -o report/figures/drawio/ report/figures/drawio/
```

Render python plots

```sh
python report/figures/plots/plot.py
```

Compile report

```sh
latexmk -lualatex -synctex=1 -interaction=nonstopmode -file-line-error -r latexmkrc --cd main.tex
```

Tip: Some times it could be helpful to remove old LaTeX build outputs before trying again:

```sh
git clean -Xf
```

Format latex source code

```sh
find -name '*.tex' -exec latexindent -wd -c=/tmp -s -l=latexindent.yaml {} \;
```

## Requirements

The project is intended to be built with the following setup:

- OS: Arch WSL (WSL 2) on Windows 11.
- Drawio: `aur/drawio-desktop`.
  - Font packages: `aur/otf-bitstream-charter`.
- TeX Live: Follow [the official installation steps](https://tug.org/texlive/quickinstall.html).
  - To get `latexindent` to work properly, I had to install a perl package or something (as described on [stackoverflow](https://stackoverflow.com/questions/19590042/error-cant-locate-file-homedir-pm-in-inc)):
    - `sudo perl -MCPAN -e 'install "File::HomeDir"'`
- Python: Version 3.11 or newer.
  - Packages: `plac`, `numpy`, `matplotlib`

## Development in VS Code

The repo is configured for efficient development in VS Code with the extensions described in [`extensions.json`](.vscode/extensions.json).
