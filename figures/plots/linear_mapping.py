#!/usr/bin/python3

import argparse
import inspect
import os
import subprocess
import time
import re
import numpy as np
import matplotlib.pyplot as plt


def r(*path):
    """
    Takes a relative path from the directory of this python file and returns the absolute path.
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)


def run_in_dir(directory, callable):
    cwd = os.getcwd()
    os.chdir(directory)
    result = callable()
    os.chdir(cwd)
    return result

def plot():
    plt.rcParams.update({'font.size': 20})
    plt.xlabel("$x$")
    plt.ylabel("$y$", rotation=0)
    plt.plot([0, 1], [10, 20], label="$l(x; 0, 1, 10, 20)$")
    plt.scatter([0.3], [13], color="tab:red", zorder=1000, label="$l(0.3; 0, 1, 10, 20)$")
    # plt.axhline(y=450, xmax=.5, color="tab:red", linestyle="dotted")
    # plt.axvline(x=1250, ymax=.45, color="tab:red", linestyle="dotted")
    # plt.plot([0, 1250], [450, 450], color="tab:red", linestyle="dotted")
    plt.legend()
