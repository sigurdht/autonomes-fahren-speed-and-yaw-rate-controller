#!/usr/bin/python3

import inspect
import os
import sys
import subprocess
import time
import plac
import numpy as np
import matplotlib.pyplot as plt
import importlib

def r(*path):
    """
    Takes a relative path from the directory of this python file and returns the absolute path.
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)


def run_in_dir(directory, callable):
    cwd = os.getcwd()
    os.chdir(directory)
    result = callable()
    os.chdir(cwd)
    return result


@plac.opt("input_dir")
@plac.opt("output_dir")
@plac.opt("file")
def plot_all(
    input_dir=r("./"),
    output_dir=r("./"),
    file=None,
):
    plt.rcParams['text.usetex'] = True
    sys.path.insert(0, input_dir)
    # Call all functions `plot_name_of_plot()` in `plot_name_of_collection.py` and store the plots they create as `name_of_collection_name_of_plot.svg`.
    filenames = [file] if file is not None else os.listdir(input_dir)
    filenames = filter(lambda filename: filename.endswith(".py") and filename != "plot.py", filenames)
    for filename in filenames:
        module_name = filename[:-len(".py")]
        module = importlib.import_module(module_name)
        def make_and_save_plot(function, plot_name):
            svg_filename = f"{plot_name}.pdf"
            plt.rcParams.update(
                {
                    "font.size": 12,
                    "font.family" : "Charter",
                }
            )
            function()
            plt.savefig(os.path.join(output_dir, svg_filename), bbox_inches='tight')
            print("Saved", svg_filename)
            plt.clf()
        for function_name in filter(lambda object_name: object_name.startswith("plot_"), dir(module)):
            plot_name = f"{module_name}_{function_name[len('plot_'):]}"
            function = getattr(module, function_name)
            make_and_save_plot(function, plot_name)
        if "plot" in dir(module):
            plot_name = module_name
            function = getattr(module, "plot")
            make_and_save_plot(function, plot_name)

if __name__ == "__main__":
    plac.call(plot_all)
