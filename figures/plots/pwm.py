#!/usr/bin/python3

import argparse
import inspect
import os
import subprocess
import time
import re
import numpy as np
import matplotlib.patches
import matplotlib.pyplot as plt


def r(*path):
    """
    Takes a relative path from the directory of this python file and returns the absolute path.
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)


def run_in_dir(directory, callable):
    cwd = os.getcwd()
    os.chdir(directory)
    result = callable()
    os.chdir(cwd)
    return result


def plot():
    plt.rcParams.update({"font.size": 16})
    plt.xticks(
        [0, 0.25, 0.4, 1],
        [
            "$0 \; \mathrm{ms}$",
            "$1.5 \; \mathrm{ms}$",
            "$T_\mathrm{P}$",
            "$20 \; \mathrm{ms}$",
        ],
    )
    plt.yticks(
        [0, 1],
        [
            "$V_\mathrm{LOW}$",
            "$V_\mathrm{HIGH}$",
        ],
    )
    plt.plot(
        [0, 0.4, 0.4, 1],
        [1, 1, 0, 0],
        label="$$u_\mathrm{PWM}$$",
    )
    plt.axvline(
        0.25,
        linestyle="--",
        color="black",
    )
    plt.axvline(
        0.22,
        linestyle=":",
        color="tab:purple",
        # label="$$\Delta T_\mathrm{P,NFE}$$",
    )
    plt.axvline(
        0.28,
        linestyle=":",
        color="tab:red",
        # label="$$\Delta T_\mathrm{P,NBE}$$",
    )
    # plt.legend(
    #     handles=[
    #         matplotlib.patches.Patch(
    #             color="none",
    #             label="$$T_\mathrm{P} = 1.5 \; \mathrm{ms} + \Delta T_\mathrm{P}$$",
    #         ),
    #         matplotlib.patches.Patch(
    #             color="none",
    #             label="$$\Delta T_\mathrm{P} \in [-0.5 \; \mathrm{ms}, +0.5 \; \mathrm{ms}]$$",
    #         ),
    #     ],
    #     handlelength=0,
    # )
    plt.annotate(
        "",
        xytext=[0.25, 0.5],
        xy=[0.4, 0.5],
        # textcoords=plt.transData,
        arrowprops=dict(arrowstyle="->"),
    )
    plt.text(0.29, 0.54, "$$\Delta T_\mathrm{P}$$")
    plt.text(0.02, 0.05, "$$\Delta T_\mathrm{P,NFE}$$", color="tab:purple")
    plt.text(0.3, 0.05, "$$\Delta T_\mathrm{P,NBE}$$", color="tab:red")
    plt.legend()


def plot_2():
    plt.rcParams.update({"font.size": 16})
    T_P = 0.7
    T_NEUTRAL = 0.35
    T_PNFE = T_NEUTRAL - 0.1
    T_PNBE = T_NEUTRAL + 0.1
    T_PNFE_ARROW_Y = 0.25
    T_PNBE_ARROW_Y = 0.25
    plt.xticks(
        [0, T_NEUTRAL, T_P, 1],
        [
            "$0 \; \mathrm{ms}$",
            "$1.5 \; \mathrm{ms}$",
            "$T_\mathrm{P}$",
            "$20 \; \mathrm{ms}$",
        ],
    )
    plt.yticks(
        [0, 1],
        [
            "$V_\mathrm{LOW}$",
            "$V_\mathrm{HIGH}$",
        ],
    )
    plt.plot(
        [0, T_P, T_P, 1],
        [1, 1, 0, 0],
        label="$$u_\mathrm{PWM}$$",
    )
    plt.axvline(
        T_NEUTRAL,
        linestyle="--",
        color="black",
    )
    plt.axvline(
        T_PNFE,
        linestyle=":",
        color="tab:purple",
        # label="$$\Delta T_\mathrm{P,NFE}$$",
    )
    plt.axvline(
        T_PNBE,
        linestyle=":",
        color="tab:red",
        # label="$$\Delta T_\mathrm{P,NBE}$$",
    )
    # plt.legend(
    #     handles=[
    #         matplotlib.patches.Patch(
    #             color="none",
    #             label="$$T_\mathrm{P} = 1.5 \; \mathrm{ms} + \Delta T_\mathrm{P}$$",
    #         ),
    #         matplotlib.patches.Patch(
    #             color="none",
    #             label="$$\Delta T_\mathrm{P} \in [-0.5 \; \mathrm{ms}, +0.5 \; \mathrm{ms}]$$",
    #         ),
    #     ],
    #     handlelength=0,
    # )
    plt.annotate(
        "",
        xytext=[T_NEUTRAL, 0.5],
        xy=[T_P, 0.5],
        arrowprops=dict(arrowstyle="->"),
    )
    plt.text(T_PNBE + 0.02, 0.54, "$$\Delta T_\mathrm{P}$$")
    plt.annotate(
        "",
        xytext=[T_NEUTRAL, T_PNFE_ARROW_Y],
        xy=[T_PNFE, T_PNFE_ARROW_Y],
        arrowprops=dict(arrowstyle="->", color="tab:purple"),
    )
    plt.text(
        T_PNFE - 0.2,
        T_PNFE_ARROW_Y - 0.02,
        "$$\Delta T_\mathrm{P,NFE}$$",
        color="tab:purple",
    )
    plt.annotate(
        "",
        xytext=[T_NEUTRAL, T_PNBE_ARROW_Y],
        xy=[T_PNBE, T_PNBE_ARROW_Y],
        arrowprops=dict(arrowstyle="->", color="tab:red"),
    )
    plt.text(
        T_PNBE + 0.02,
        T_PNBE_ARROW_Y - 0.02,
        "$$\Delta T_\mathrm{P,NBE}$$",
        color="tab:red",
    )
    plt.legend()
