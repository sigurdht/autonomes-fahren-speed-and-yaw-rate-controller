#!/usr/bin/python3

import argparse
import inspect
import os
import subprocess
import time
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
import scipy.stats


def r(*path):
    """
    Takes a relative path from the directory of this python file and returns the absolute path.
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)


def run_in_dir(directory, callable):
    cwd = os.getcwd()
    os.chdir(directory)
    result = callable()
    os.chdir(cwd)
    return result


def plot_drv_responsive():
    plt.rcParams.update({"font.size": 12})
    results_file = r("data/automated_outputs/drv_response_time/results_responsive.csv")
    with open(results_file) as file:
        results = np.array(
            [
                [float(value) for value in line.split(",")]
                for line in filter(lambda line: line != "", file.read().split("\n"))
            ]
        )
        delay = results[:, 0]
        response_time = results[:, 1]
    plt.xlabel("$d$ [ms]")
    plt.ylabel("$R_\mathrm{m}$ [ms]")
    plt.scatter(delay, response_time, color="black")
    print("Min:", np.min(response_time))
    print("Max:", np.max(response_time))
    print("Avergage:", np.average(response_time))
    plt.axhline(40, linestyle="dashed", color="black", label="Deadline")


def plot_drv_responsive_modulo():
    plt.rcParams.update({"font.size": 12})
    results_file = r("data/automated_outputs/drv_response_time/results_responsive.csv")
    with open(results_file) as file:
        results = np.array(
            [
                [float(value) for value in line.split(",")]
                for line in filter(lambda line: line != "", file.read().split("\n"))
            ]
        )
        delay = results[:, 0]
        response_time = results[:, 1]
        delay %= 15.95

    delay_filtered = []
    response_time_filtered = []
    for i in range(len(delay)):
        if delay[i] > 10 and response_time[i] > 14:
            continue
        delay_filtered.append(delay[i])
        response_time_filtered.append(response_time[i])

    plt.xlabel(r"$d \bmod 15.95$ [ms]")
    plt.ylabel("$R_\mathrm{m}$ [ms]")
    plt.scatter(delay, response_time, color="black", label="Measurements")
    regression_result = scipy.stats.linregress(delay_filtered, response_time_filtered)
    plt.plot(
        [0, 15.95],
        [
            regression_result.intercept,
            regression_result.intercept + regression_result.slope * 15.95,
        ],
        color="tab:red",
        linestyle="solid",
        label="Linear regression",
    )
    print("Slope:", regression_result.slope)
    plt.legend()


def plot_drv_responsive_static_modulo():
    plt.rcParams.update({"font.size": 12})
    results_file = r(
        "data/automated_outputs/drv_response_time/results_responsive_static.csv"
    )
    with open(results_file) as file:
        results = np.array(
            [
                [float(value) for value in line.split(",")]
                for line in filter(lambda line: line != "", file.read().split("\n"))
            ]
        )
        delay = results[:, 0]
        response_time = results[:, 1]
        delay %= 15.95

    delay_filtered = []
    response_time_filtered = []
    for i in range(len(delay)):
        if delay[i] > 10 and response_time[i] > 14:
            continue
        delay_filtered.append(delay[i])
        response_time_filtered.append(response_time[i])

    plt.xlabel(r"$d_\mathrm{s} \bmod 15.95$ [ms]")
    plt.ylabel(r"$R_\mathrm{m}$ [ms]")
    plt.scatter(delay, response_time, color="black", label="Measurements")
    regression_result = scipy.stats.linregress(delay_filtered, response_time_filtered)
    plt.plot(
        [0, 15.95],
        [
            regression_result.intercept,
            regression_result.intercept + regression_result.slope * 15.95,
        ],
        color="tab:red",
        linestyle="solid",
        label="Linear regression",
    )
    print("Slope:", regression_result.slope)
    plt.legend()


def plot_drv_unresponsive():
    plt.rcParams.update({"font.size": 12})
    results_file = r(
        "data/automated_outputs/drv_response_time/results_unresponsive.csv"
    )
    with open(results_file) as file:
        results = np.array(
            [
                [float(value) for value in line.split(",")]
                for line in filter(lambda line: line != "", file.read().split("\n"))
            ]
        )
        delay = results[:, 0]
        response_time = results[:, 1]
    plt.xlabel("$d$ [ms]")
    plt.ylabel("$R_\mathrm{m}$ [ms]")
    plt.scatter(delay, response_time, color="black")
    plt.axhline(40, linestyle="dashed", color="black", label="Deadline")
    print("Min:", np.min(response_time))
    print("Max:", np.max(response_time))
    print("Avergage:", np.average(response_time))


def plot_drv_fast_responsive():
    plt.rcParams.update({"font.size": 15})
    results_file = r(
        "data/automated_outputs/drv_response_time/results_fast_responsive.csv"
    )
    with open(results_file) as file:
        results = np.array(
            [
                [float(value) for value in line.split(",")]
                for line in filter(lambda line: line != "", file.read().split("\n"))
            ]
        )
        delay = results[:, 0]
        response_time = results[:, 1]
    plt.xlabel("$d$ [ms]")
    plt.ylabel("$R_\mathrm{m}$ [ms]")
    plt.scatter(delay, response_time, color="black", label="Measurements")
    print("Min:", np.min(response_time))
    print("Max:", np.max(response_time))
    print("Avergage:", np.average(response_time))
    print(
        "Percentage d < 500 ms:",
        100 * len(list(filter(lambda d: d < 500, delay))) / len(delay),
        "%",
    )
    plt.axhline(40, linestyle="dashed", color="black", label="Deadline")
    plt.legend()


def plot_drv_fast_responsive_modulo():
    plt.rcParams.update({"font.size": 15})
    results_file = r(
        "data/automated_outputs/drv_response_time/results_fast_responsive.csv"
    )
    with open(results_file) as file:
        results = np.array(
            [
                [float(value) for value in line.split(",")]
                for line in filter(lambda line: line != "", file.read().split("\n"))
            ]
        )
        delay = results[:, 0]
        response_time = results[:, 1]
        modulo_value = 1
        delay %= modulo_value

    delay_filtered = []
    response_time_filtered = []
    for i in range(len(delay)):
        if delay[i] > 10 and response_time[i] > 14:
            continue
        delay_filtered.append(delay[i])
        response_time_filtered.append(response_time[i])

    plt.xlabel(rf"$d \bmod {modulo_value}$ [ms]")
    plt.ylabel("$R_\mathrm{m}$ [ms]")
    plt.scatter(delay, response_time, color="black", label="Measurements")
    regression_result = scipy.stats.linregress(delay_filtered, response_time_filtered)
    plt.plot(
        [0, modulo_value],
        [
            regression_result.intercept,
            regression_result.intercept + regression_result.slope * modulo_value,
        ],
        color="tab:red",
        linestyle="solid",
        label="Linear regression",
    )
    print("Slope:", regression_result.slope)
    plt.legend()


def plot_drv_fast_unresponsive():
    plt.rcParams.update({"font.size": 15})
    results_file = r(
        "data/automated_outputs/drv_response_time/results_fast_unresponsive.csv"
    )
    with open(results_file) as file:
        results = np.array(
            [
                [float(value) for value in line.split(",")]
                for line in filter(lambda line: line != "", file.read().split("\n"))
            ]
        )
        delay = results[:, 0]
        response_time = results[:, 1]
    plt.xlabel("$d$ [ms]")
    plt.ylabel("$R_\mathrm{m}$ [ms]")
    plt.scatter(delay, response_time, color="black", label="Measurements")
    print("Min:", np.min(response_time))
    print("Max:", np.max(response_time))
    print("Avergage:", np.average(response_time))
    print(
        "Percentage d < 500 ms:",
        100 * len(list(filter(lambda d: d < 500, delay))) / len(delay),
        "%",
    )
    plt.axhline(40, linestyle="dashed", color="black", label="Deadline")
    plt.legend()


def plot_drv_fast_old():
    plt.rcParams.update({"font.size": 15})
    results_file = r("data/automated_outputs/drv_response_time/results_fast_old.csv")
    with open(results_file) as file:
        results = np.array(
            [
                [float(value) for value in line.split(",")]
                for line in filter(lambda line: line != "", file.read().split("\n"))
            ]
        )
        delay = results[:, 0]
        response_time = results[:, 1]
    plt.xlabel("$d$ [ms]")
    plt.ylabel("$R_\mathrm{m}$ [ms]")
    plt.scatter(delay, response_time, color="black", label="Measurements")
    print("Min:", np.min(response_time))
    print("Max:", np.max(response_time))
    print("Avergage:", np.average(response_time))
    print(
        "Percentage d < 500 ms:",
        100 * len(list(filter(lambda d: d < 500, delay))) / len(delay),
        "%",
    )
    plt.axhline(40, linestyle="dashed", color="black", label="Deadline")
    plt.legend()
