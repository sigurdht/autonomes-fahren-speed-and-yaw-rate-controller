#!/usr/bin/python3

import argparse
import inspect
import os
import subprocess
import time
import re
import numpy as np
import matplotlib.patches
import matplotlib.pyplot as plt


def r(*path):
    """
    Takes a relative path from the directory of this python file and returns the absolute path.
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)


def run_in_dir(directory, callable):
    cwd = os.getcwd()
    os.chdir(directory)
    result = callable()
    os.chdir(cwd)
    return result


def parse_data_from_serial_output(serial_output_file, group_config: dict):
    result = {
        group_number: {channel: [] for channel in channels}
        for group_number, channels in group_config.items()
    }
    with open(serial_output_file) as file:
        for i, line in enumerate(filter(lambda line: line[0] == "#", file.readlines())):
            if "\x02" in line:
                continue
            group_number_match = re.compile(r"#(\d+):").search(line)
            if group_number_match is None or group_number_match.start() != 0:
                # raise Exception(
                #     f"Line {i} starts with # but has no recognizable group number"
                # )
                continue
            group_number = group_number_match.group(1)
            if group_number not in group_config.keys():
                continue
            channel_values = line[group_number_match.end() :].split(" | ")
            if len(channel_values) != len(group_config[group_number]):
                continue
            for i, value in enumerate(channel_values):
                try:
                    value = float(value)
                except ValueError:
                    value = None
                result[group_number][group_config[group_number][i]].append(value)
    return result


HALL_SENSOR_DELTA_S = 0.204204  # Diameter of wheel in meters


def make_step_response_plot(
    seconds,
    requested_speed=None,
    speed=None,
    cruise_drvval=None,
    hall_dt=None,
    hall_dt8=None,
    requested_speed_color="black",
    speed_color="tab:blue",
    cruise_drvval_color="tab:orange",
    hall_dt_color="tab:red",
    hall_dt8_color="tab:green",
    legend=True,
):
    fig, ax = plt.subplots()
    ax.set_xlabel("time [s]")
    ax.set_ylabel("speed [m/s]")
    lines = []
    if requested_speed is not None:
        lines += ax.plot(
            seconds,
            requested_speed,
            label="requested speed",
            color=requested_speed_color,
            linestyle="dashed",
        )
    if hall_dt is not None or hall_dt8 is not None:
        hall_dt_ax = ax.twinx()
        hall_dt_ax.set_ylabel("hall DT [ms]")
        if cruise_drvval is not None:
            hall_dt_ax.spines["right"].set_position(("outward", 60))
        if hall_dt is not None:
            lines += hall_dt_ax.plot(
                seconds, hall_dt / 1e-3, label="hall DT", color=hall_dt_color
            )
        if hall_dt8 is not None:
            lines += hall_dt_ax.plot(
                seconds, hall_dt8 / 1e-3, label="hall DT8", color=hall_dt8_color
            )

    if speed is not None:
        lines += ax.plot(seconds, speed, label="measured speed", color=speed_color)
    if cruise_drvval is not None:
        drvval_ax = ax.twinx()
        drvval_ax.set_ylabel("drvval [0-1000]")
        # drvval_ax.tick_params(axis="y", labelcolor="tab:orange")
        lines += drvval_ax.plot(
            seconds, cruise_drvval, label="cruise drvval", color=cruise_drvval_color
        )

    # ax.set_xlim(5, 30)
    # ax.set_ylim(0, .75)
    if legend:
        plt.legend(lines, [line.get_label() for line in lines])


def plot_step_response():
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_1.txt"),
    #     {
    #         "4": [
    #             "_tics",
    #             "cruise_drvval",
    #             "hall_cnt",
    #             "hall_dt",
    #             "hall_dt8"
    #         ],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_no_cruise.txt"),
    #     {
    #         "4": [
    #             "_tics",
    #             # "cruise_drvval",
    #             "hall_cnt",
    #             "hall_dt",
    #             "hall_dt8"
    #         ],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_no_cruise_2.txt"),
    #     {
    #         "4": [
    #             "_tics",
    #             # "cruise_drvval",
    #             "hall_cnt",
    #             "hall_dt",
    #             "hall_dt8"
    #         ],
    #     },
    # )
    data = parse_data_from_serial_output(
        r("data/serial_output_old/serial_output_2.txt"),
        {
            "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
        },
    )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff_only.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    data = parse_data_from_serial_output(
        r("data/serial_output_old/serial_output_ff+p0.1.txt"),
        {
            "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
        },
    )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p0.1_500mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p0.05_500mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p0.2_500mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p0.5_500mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p0.5.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p0.5_2000mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p0.5+i0.5_2000mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p0.5+i0.1_2000mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p0.5+i0.02_2000mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p1.0+i0.1_2000mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p2.0+i0.2_2000mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p2.0+i0.2_500mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p1.0+i0.05_500mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p1.0+i0.05.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output_ff+p1.0+i0.05_2000mmps.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )
    # data = parse_data_from_serial_output(
    #     r("data/serial_output_old/serial_output.txt"),
    #     {
    #         "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
    #     },
    # )

    requested_speed = 1.0
    speed_dt1 = (HALL_SENSOR_DELTA_S / 8) / (np.array(data["4"]["hall_dt"]) * 1e-4)
    speed = HALL_SENSOR_DELTA_S / (np.array(data["4"]["hall_dt8"]) * 1e-3)
    # The first speed value from data does not represent the actual speed, since there are just 0s in the
    # rolling delta t buffer, and this wrong speed is not the one that is used by the cruise controller either.
    speed[0] = 0

    make_step_response_plot(
        seconds=(np.array(data["4"]["_tics"]) - data["4"]["_tics"][0]) * 1e-3,
        requested_speed=np.linspace(
            requested_speed, requested_speed, len(data["4"]["_tics"])
        ),
        # requested_speed=speed_dt1,
        speed=speed,
        cruise_drvval=np.array(data["4"]["cruise_drvval"]),
        # cruise_drvval=np.linspace(50, 50, len(data["4"]["_tics"])),
        # hall_dt=np.array(data["4"]["hall_dt"]) * 8e-4,
        # hall_dt8=np.array(data["4"]["hall_dt8"]) * 1e-3,
    )


def plot_step_response_integral_only():
    data = parse_data_from_serial_output(
        r("data/serial_output_old/serial_output_2.txt"),
        {
            "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
        },
    )

    requested_speed = 1.0
    speed_dt1 = (HALL_SENSOR_DELTA_S / 8) / (np.array(data["4"]["hall_dt"]) * 1e-4)
    speed = HALL_SENSOR_DELTA_S / (np.array(data["4"]["hall_dt8"]) * 1e-3)
    # The first speed value from data does not represent the actual speed, since there are just 0s in the
    # rolling delta t buffer, and this wrong speed is not the one that is used by the cruise controller either.
    speed[0] = 0

    make_step_response_plot(
        seconds=(np.array(data["4"]["_tics"]) - data["4"]["_tics"][0]) * 1e-3,
        requested_speed=np.linspace(
            requested_speed, requested_speed, len(data["4"]["_tics"])
        ),
        # requested_speed=speed_dt1,
        speed=speed,
        speed_color="tab:orange",
        # cruise_drvval=np.array(data["4"]["cruise_drvval"]),
        # cruise_drvval=np.linspace(50, 50, len(data["4"]["_tics"])),
        # hall_dt=np.array(data["4"]["hall_dt"]) * 8e-4,
        # hall_dt8=np.array(data["4"]["hall_dt8"]) * 1e-3,
        legend=False,
    )


def plot_feed_forward_reference():
    with open(
        r("data/automated_outputs/find_feed_forward_values/results_car4.csv")
    ) as file:
        data = np.array(
            [
                [float(value) for value in line.split(",")]
                for line in filter(lambda line: line != "", file.read().split("\n"))
            ]
        )
    speed = data[:, 1] * 1000
    drivevalue = data[:, 0]
    plt.xlabel(r"$v \; \mathrm{[mm/s]}$")
    plt.ylabel(r"$u$", rotation=0)
    plt.plot(speed, drivevalue, marker="o", label=r"$(V_n, U_n)$")
    plt.legend()


def plot_feed_forward_reference_example():
    plt.rcParams.update({"font.size": 16})
    speed = np.array([0, 500, 1000, 1500, 2000, 2500])
    drivevalue = np.array([0, 50, 150, 300, 600, 1000])
    plt.xlabel(r"$v \; \mathrm{[mm/s]}$")
    plt.ylabel(r"$u$", rotation=0)
    plt.plot(speed, drivevalue, marker="o", label=r"$(V_n, U_n)$")
    plt.scatter(
        [1250],
        [225],
        color="tab:red",
        zorder=1000,
        label="$(v_\mathrm{r}, u_\mathrm{FF})$",
    )
    # plt.axhline(y=450, xmax=.5, color="tab:red", linestyle="dotted")
    # plt.axvline(x=1250, ymax=.45, color="tab:red", linestyle="dotted")
    # plt.plot([0, 1250], [450, 450], color="tab:red", linestyle="dotted")
    plt.legend()


def compare_step_response(t_s, k_p, k_i):
    acceleration_times = []
    for requested_speed in [500, 1000, 1500]:
        data = parse_data_from_serial_output(
            r(
                f"data/automated_outputs/cruise_controller_step_response_car4/serial_output_ts{t_s}_ff+p{k_p}+i{k_i}_{requested_speed}mmps.txt"
            ),
            {
                "4": ["_tics", "cruise_drvval", "hall_cnt", "hall_dt", "hall_dt8"],
            },
        )

        speed_dt1 = (HALL_SENSOR_DELTA_S / 8) / (np.array(data["4"]["hall_dt"]) * 1e-4)
        speed = HALL_SENSOR_DELTA_S / (np.array(data["4"]["hall_dt8"]) * 1e-3)
        # The first speed value from data does not represent the actual speed, since there are just 0s in the
        # rolling delta t buffer, and this wrong speed is not the one that is used by the cruise controller either.
        speed[0] = 0
        seconds = (np.array(data["4"]["_tics"]) - data["4"]["_tics"][0]) * 1e-3

        # plt.rcParams.update({"font.size": 10, "font.family" : "Charter"})
        # subplots = plt.subplots(nrows=3, ncols=3)
        # print(requested_speed)
        # print(len(seconds))
        # print(len(speed))
        requested_speed /= 1000

        acceleration_time_index, _ = next(
            filter(
                lambda index_speed: index_speed[1] >= requested_speed, enumerate(speed)
            )
        )
        acceleration_time = seconds[acceleration_time_index]
        acceleration_times.append(acceleration_time)

        # print(
        #     "acceleration_time:",
        #     acceleration_time,
        #     acceleration_time_index,
        #     requested_speed,
        # )

        plt.plot(seconds, speed)
        plt.plot(
            [0, 10],
            [requested_speed, requested_speed],
            linestyle="dashed",
            color="black",
        )
        plt.xlabel(r"$$t \; [\mathrm{s}]$$")
        plt.ylabel(r"$$v_\mathrm{m} \; [\mathrm{m/s}]$$")
        plt.legend(
            handles=[
                matplotlib.patches.Patch(
                    color="none", label=f"$$T_\mathrm{{S}} = {t_s} \; \mathrm{{ms}}$$"
                ),
                matplotlib.patches.Patch(
                    color="none",
                    label=f"$$K_\mathrm{{P}} = {k_p} \; \mathrm{{(m/s)^{{-1}}}}$$",
                ),
                matplotlib.patches.Patch(
                    color="none",
                    label=f"$$K_\mathrm{{I}} = {k_i} \; \mathrm{{m^{{-1}}}}$$",
                ),
            ],
            handlelength=0,
        )
    print("Average acceleration time:", sum(acceleration_times) / 3)


def plot_compare_step_response():
    t_s = 50
    # for k_p in [0, 50, 100, 200, 500, 1000, 2000]
    k_p = 500
    # for k_i in [0, 5, 10, 20, 50, 100, 200, 500]
    k_i = 10
    compare_step_response(t_s=t_s, k_p=k_p, k_i=k_i)


def plot_compare_step_response_ff_only():
    compare_step_response(t_s=50, k_p=0, k_i=0)


def plot_compare_step_response_ffp():
    compare_step_response(t_s=50, k_p=500, k_i=0)


def plot_compare_step_response_kp_too_great():
    compare_step_response(t_s=50, k_p=2000, k_i=10)


def plot_compare_step_response_ki_too_great():
    compare_step_response(t_s=50, k_p=500, k_i=500)

    # make_step_response_plot(
    #     seconds=(np.array(data_500["4"]["_tics"]) - data_500["4"]["_tics"][0]) * 1e-3,
    #     seconds_b=(np.array(data_1000["4"]["_tics"]) - data_1000["4"]["_tics"][0]) * 1e-3,
    #     seconds_c=(np.array(data_1500["4"]["_tics"]) - data_1500["4"]["_tics"][0]) * 1e-3,
    #     requested_speed=np.linspace(
    #         requested_speed, requested_speed, len(data["4"]["_tics"])
    #     ),
    #     # requested_speed=speed_dt1,
    #     speed=speed,
    #     # cruise_drvval=np.array(data["4"]["cruise_drvval"]),
    #     # cruise_drvval=np.linspace(50, 50, len(data["4"]["_tics"])),
    #     # hall_dt=np.array(data["4"]["hall_dt"]) * 8e-4,
    #     # hall_dt8=np.array(data["4"]["hall_dt8"]) * 1e-3,
    # )
