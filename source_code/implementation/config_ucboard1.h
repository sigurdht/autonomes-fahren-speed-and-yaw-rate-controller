/*
 * config_ucboard1.h
 *
 * Defines config to use when compiling for ucboard1.
 */

#ifndef CONFIG_UCBOARD1_H
#define CONFIG_UCBOARD1_H

// Number of LEDs on the car
#define CONFIG_LED_COUNT 6

// Name of LEDs, used for identification in serial protocol
#define CONFIG_LED_NAMES \
    { "A", "B", "C", "D", "E", "F" }

// A struct containing information about which GPIO ports and pins the LEDs use
#define CONFIG_LED_PORTS_AND_PINS                                                           \
    {                                                                                       \
        {GPIOC, GPIO_PIN_4}, {GPIOC, GPIO_PIN_5}, {GPIOE, GPIO_PIN_0}, {GPIOE, GPIO_PIN_1}, \
            {GPIOE, GPIO_PIN_3}, {                                                          \
            GPIOE, GPIO_PIN_4                                                               \
        }                                                                                   \
    }

// Enables support for encoder (Inkrementalgeber) for speed measurment
#define CONFIG_ENCODER_ENABLE 0

// Enables support for hall sensor for speed measurement
#define CONFIG_HALL_SENSOR_ENABLE 1

// Defines how far the car moves per 8 hall sensor pulses (unit: um)
#define CONFIG_HALL_SENSOR_DELTA_S 204204

// Defines which USART of the STM32F303 to use
// Possible values are: 2 (USART2), 3 (USART3), 4 (UART4)
#define CONFIG_USART 2  // USB
// #define CONFIG_USART 4  // Bluetooth

// Defines the baud rate used for the USART
#define CONFIG_USART_BAUD_RATE 921600  // USB
// #define CONFIG_USART_BAUD_RATE 57600  // Bluetooth

// Type of inertial measurement unit
#define CONFIG_IMU_TYPE_MPU9250  1
#define CONFIG_IMU_TYPE_ICM20648 0

// Enables support for receiver for remote control
#define CONFIG_RECEIVER_ENABLE 0

// Type of ultrasonic sensors
#define CONFIG_US_TYPE_SRF08 1
#define CONFIG_US_TYPE_SRF10 0

// Number of ultrasonic sensors on the car
#define CONFIG_US_COUNT 4

// I2C bus addresses for ultrasonic sensors
#define CONFIG_US_ADDRESSES \
    { 0xE0, 0xE2, 0xE4, 0xE6 }

// DAQ channel names for ultrasonic sensors
#define CONFIG_US_DAQ_CHANNEL_NAMES \
    { "USL", "USF", "USR", "USB" }

// DAQ channel descriptions for ultrasonic sensors
#define CONFIG_US_DAQ_CHANNEL_DESCRIPTIONS                                                    \
    {                                                                                         \
        "ultrasonic left distance", "ultrasonic front distance", "ultrasonic right distance", \
            "ultrasonic back distance"                                                        \
    }

#define CONFIG_MOTOR_FULLFORWARDS            -500
#define CONFIG_MOTOR_NEUTRAL_FORWARDS_ENDED  -52
#define CONFIG_MOTOR_FULLBACKWARDS           250
#define CONFIG_MOTOR_NEUTRAL_BACKWARDS_ENDED 53
#define CONFIG_MOTOR_PWMVAL_DELTA_NEGATIVE   0

// Delay between each time the cruise controller adjusts the drvval (unit: ms)
#define CONFIG_CRUISE_CONTROL_T_S 50

// Estimated top speed for the configuration of car and motor (unit: mm/s)
// Used for calibrating the cruise controller.
#define CONFIG_CRUISE_CONTROL_REFERENCE_TOP_SPEED_FORWARDS  (5000)
#define CONFIG_CRUISE_CONTROL_REFERENCE_TOP_SPEED_BACKWARDS (2500)

// Reference {speed, drvval} pairs for the cruise controller's feed forward term (unit: mm/s)
// The list must have minimum 2 entries.
// All values must be integers.
// Entries must be ordered by increasing speed.
// Drvval values should always be given as positive numbers. Driving direction is interpreted
// automatically.
#define CONFIG_CRUISE_CONTROL_FF_REFERENCE                                                      \
    {                                                                                           \
        {-803, -500}, {-591, -400}, {-393, -300}, {-172, -200}, {0, 0}, {306, 200}, {585, 300}, \
            {822, 400}, {1041, 500}, {1305, 600}, {1535, 700}, {1729, 800}, {1827, 900}, {      \
            1868, 1000                                                                          \
        }                                                                                       \
    }
#define CONFIG_CRUISE_CONTROL_FF_REFERENCE_LENGTH 14

// Multiplication factor for the cruise controller's proportional term (unit: drvval/(m/s))
#define CONFIG_CRUISE_CONTROL_K_P 1000

// Multiplication factor for the cruise controller's integral term (unit: drvval/(m))
#define CONFIG_CRUISE_CONTROL_K_I 100

// Type of motor(s)
#define CONFIG_MOTOR_TYPE_PWM  1
#define CONFIG_MOTOR_TYPE_BLDC 0

#endif /* CONFIG_UCBOARD1_H */
