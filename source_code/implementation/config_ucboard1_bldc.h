﻿/*
 * config_ucboard1_bldc.h
 *
 * Defines config to use when compiling for ucboard1 with BLDC motor.
 */

#ifndef CONFIG_UCBOARD1_BLDC_H
#define CONFIG_UCBOARD1_BLDC_H

#include "config_ucboard1.h"

#undef CONFIG_MOTOR_TYPE_PWM
#define CONFIG_MOTOR_TYPE_PWM 0

#undef CONFIG_MOTOR_TYPE_BLDC
#define CONFIG_MOTOR_TYPE_BLDC 1

#endif /* CONFIG_UCBOARD1_BLDC_H */
