﻿/*
 * config_ucboard1.h
 *
 * Defines config to use when compiling for ucboard2.
 */

#ifndef CONFIG_UCBOARD2_H
#define CONFIG_UCBOARD2_H

// Number of LEDs on the car
#define CONFIG_LED_COUNT 9

// Name of LEDs, used for identification in serial protocol
#define CONFIG_LED_NAMES \
    { "A", "B", "U1", "U2", "U3", "U4", "BLKR", "BLKL", "BRMS" }

// A struct containing information about which GPIO ports and pins the LEDs use
#define CONFIG_LED_PORTS_AND_PINS                                                                  \
    {                                                                                              \
        {GPIOC, GPIO_PIN_4}, {GPIOC, GPIO_PIN_5}, {GPIOD, GPIO_PIN_10}, {GPIOD, GPIO_PIN_11},      \
            {GPIOD, GPIO_PIN_13}, {GPIOB, GPIO_PIN_5}, {GPIOD, GPIO_PIN_3}, {GPIOD, GPIO_PIN_4}, { \
            GPIOD, GPIO_PIN_5                                                                      \
        }                                                                                          \
    }

// Enables support for encoder (Inkrementalgeber) for speed measurment
#define CONFIG_ENCODER_ENABLE 1

// Enables support for hall sensor for speed measurement
#define CONFIG_HALL_SENSOR_ENABLE 0

// Defines how far the car moves per 8 hall sensor pulses (unit: um)
#define CONFIG_HALL_SENSOR_DELTA_S 0

// Defines which USART of the STM32F303 to use
// Possible values are: 2 (USART2), 3 (USART3), 4 (UART4)
#define CONFIG_USART 2  // USB

// Defines the baud rate used for the USART
#define CONFIG_USART_BAUD_RATE 921600  // USB

// Type of inertial measurement unit
#define CONFIG_IMU_TYPE_MPU9250  0
#define CONFIG_IMU_TYPE_ICM20648 1

// Enables support for receiver for remote control
#define CONFIG_RECEIVER_ENABLE 1

// Type of ultrasonic sensors
#define CONFIG_US_TYPE_SRF08 0
#define CONFIG_US_TYPE_SRF10 1

// Number of ultrasonic sensors on the car
#define CONFIG_US_COUNT 8

// I2C bus addresses for ultrasonic sensors
#define CONFIG_US_ADDRESSES \
    { 0xE0, 0xE2, 0xE4, 0xE6, 0xE8, 0xEA, 0xEC, 0xEE }

// DAQ channel names for ultrasonic sensors
#define CONFIG_US_DAQ_CHANNEL_NAMES \
    { "US1", "US2", "US3", "US4", "US5", "US6", "US7", "US8" }

// DAQ channel descriptions for ultrasonic sensors
#define CONFIG_US_DAQ_CHANNEL_DESCRIPTIONS                                             \
    {                                                                                  \
        "ultrasonic 1 distance", "ultrasonic 2 distance", "ultrasonic 3 distance",     \
            "ultrasonic 4 distance", "ultrasonic 5 distance", "ultrasonic 6 distance", \
            "ultrasonic 7 distance", "ultrasonic 8 distance"                           \
    }

#define CONFIG_MOTOR_FULLFORWARDS            -500
#define CONFIG_MOTOR_NEUTRAL_FORWARDS_ENDED  -38
#define CONFIG_MOTOR_FULLBACKWARDS           500
#define CONFIG_MOTOR_NEUTRAL_BACKWARDS_ENDED 74
#define CONFIG_MOTOR_PWMVAL_DELTA_NEGATIVE   1

// Delay between each time the cruise controller adjusts the drvval (unit: ms)
#define CONFIG_CRUISE_CONTROL_T_S 1000

// Estimated top speed for the configuration of car and motor (unit: mm/s)
// Used for calibrating the cruise controller.
#define CONFIG_CRUISE_CONTROL_REFERENCE_TOP_SPEED_FORWARDS  (5000)
#define CONFIG_CRUISE_CONTROL_REFERENCE_TOP_SPEED_BACKWARDS (2500)

#endif /* CONFIG_UCBOARD2_H */
