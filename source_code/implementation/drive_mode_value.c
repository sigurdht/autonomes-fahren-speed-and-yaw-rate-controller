typedef enum EnDrvMode_ {
    DRVMODE_OFF,
    DRVMODE_FORWARDS,
    DRVMODE_BACKWARDS,
    DRVMODE_DIRECT,
    DRVMODE_RC,
    DRVMODE_CRUISE,
} EnDrvMode_t;

typedef struct {
    EnDrvMode_t eMode;
    int32_t iValue;
} DriveModeValue_t;
