static bool s_bDelayDone = false;
if (f_aNewLedSeq[0].mode == LEDMODE_ON) {
    if (!s_bDelayDone) {
        volatile int foo;
        for (uint64_t i = 0; i < 2000000; i++) {
            foo = 42;
        }
        s_bDelayDone = true;
    }
}
else {
    s_bDelayDone = false;
}
