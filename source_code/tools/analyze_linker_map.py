#!/usr/bin/python3

import argparse
import inspect
import os
import subprocess
import time
import re
from dataclasses import dataclass


def r(*path):
    """
    Takes a relative path from the directory of this python file and returns the absolute path.
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)


def run_in_dir(directory, callable):
    cwd = os.getcwd()
    os.chdir(directory)
    result = callable()
    os.chdir(cwd)
    return result


def main(linker_map_path):
    with open(linker_map_path) as file:
        linker_map = file.read()

    linker_map = linker_map[linker_map.index("Linker script and memory map") :]

    def get_section_size(section):
        match re.search(
            r"\n(" + re.escape(section) + r")\s+(0x[0-9a-f]+)\s+(0x[0-9a-f]+)",
            linker_map,
        ):
            case None:
                raise Exception(f"Section {section} was not found")
            case m:
                return int(m.group(3), 16)

    def format_bytes(num_of_bytes):
        if num_of_bytes < 1024:
            return f"{num_of_bytes} B"
        elif num_of_bytes < 1024**2:
            return f"{num_of_bytes/1024:.1f} KiB"

    text_size = get_section_size(".text")
    rodata_size = get_section_size(".rodata")
    data_size = get_section_size(".data")
    bss_size = get_section_size(".bss")
    main_stack_size = 0x1000

    @dataclass
    class SpecialStaticVariable:
        description: str
        variable_name: str

        def size(self):
            try:
                return get_section_size(f" .bss.{self.variable_name}")
            except:
                return 0

    special_static_variables = [
        SpecialStaticVariable(
            description="idle task stack", variable_name="f_xIdleTaskStack"
        ),
        SpecialStaticVariable(
            description="timer task stack", variable_name="f_xTimerTaskStack"
        ),
        SpecialStaticVariable(
            description="motor controller stack",
            variable_name="f_axMotorControllerTaskStack",
        ),
        SpecialStaticVariable(
            description="comm rx task stack",
            variable_name="f_comm_rx_do_systickTaskStack",
        ),
        SpecialStaticVariable(
            description="comm tx task stack",
            variable_name="f_comm_tx_do_systickTaskStack",
        ),
    ]

    flash_size = 512 * 1024
    flash_used = text_size + rodata_size
    ram_size = 64 * 1024
    ram_used = data_size + bss_size + main_stack_size
    ccram_size = 16 * 1024
    ccram_used = 0

    print(f'+{"-"*24}+{"-"*24}+{"-"*24}+')
    print(f'|{f"FLASH":^24}|{f"program ":>24}|{f"{format_bytes(text_size)} ":>24}|')
    print(f'|{"":^24}|{f"constants ":>24}|{f"{format_bytes(rodata_size)} ":>24}|')
    print(f'|{" "*24}+{"-"*24}+{"-"*24}+')
    print(f'|{"":^24}|{f"total used ":>24}|{f"{format_bytes(flash_used)} ":>24}|')
    print(
        f'|{"":^24}|{f"not used ":>24}|{f"{format_bytes(flash_size - flash_used)} ":>24}|'
    )
    print(f'+{"-"*24}+{"-"*24}+{"-"*24}+')
    print(
        f'|{f"RAM":^24}|{f"static variables ":>24}|{f"{format_bytes(data_size + bss_size - sum([variable.size() for variable in special_static_variables]))} ":>24}|'
    )
    for variable in special_static_variables:
        print(
            f'|{"":^24}|{f"{variable.description} ":>24}|{f"{format_bytes(variable.size())} ":>24}|'
        )
    print(f'|{"":^24}|{f"ISR stack ":>24}|{f"{format_bytes(main_stack_size)} ":>24}|')
    print(f'|{" "*24}+{"-"*24}+{"-"*24}+')
    print(f'|{"":^24}|{f"total ":>24}|{f"{format_bytes(ram_used)} ":>24}|')
    print(
        f'|{"":^24}|{f"not used ":>24}|{f"{format_bytes(ram_size - ram_used)} ":>24}|'
    )
    print(f'+{"-"*24}+{"-"*24}+{"-"*24}+')
    print(f'|{f"CCRAM":^24}|{f"total ":>24}|{f"{format_bytes(ccram_used)} ":>24}|')
    print(
        f'|{"":^24}|{f"not used ":>24}|{f"{format_bytes(ccram_size - ccram_used)} ":>24}|'
    )
    print(f'+{"-"*24}+{"-"*24}+{"-"*24}+')
    print()

    print("32 largest static variables")
    print(f'+{"-"*32}+{"-"*12}+')
    total_size_highest_32 = 0
    for variable_name, address, size, file_path in sorted(
        (
            re.findall(
                r"\n \.bss\.([\w\.]+)\s+(0x[0-9a-f]+)\s+(0x[0-9a-f]+) (.+)", linker_map
            )
            + re.findall(
                r"\n \.data\.([\w\.]+)\s+(0x[0-9a-f]+)\s+(0x[0-9a-f]+) (.+)", linker_map
            )
        ),
        key=lambda m: int(m[2], 16),
        reverse=True,
    )[:32]:
        if variable_name in [
            variable.variable_name for variable in special_static_variables
        ]:
            continue
        size = int(size, 16)
        total_size_highest_32 += size
        if file_path.startswith("CMakeFiles/pses_ucboard.dir/"):
            file_path = file_path[len("CMakeFiles/pses_ucboard.dir/") :]
        print(f'|{f" {variable_name}":<32}|{f"{format_bytes(size)} ":>12}| {file_path}')
    print(f'|{f" total":<32}|{f"{format_bytes(total_size_highest_32)} ":>12}|')
    print(f'+{"-"*32}+{"-"*12}+')


if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser()
    for parameter in inspect.signature(main).parameters:
        argument_parser.add_argument(parameter)
    arguments = argument_parser.parse_args()
    main(**vars(arguments))
