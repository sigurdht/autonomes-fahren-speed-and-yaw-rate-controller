#!/usr/bin/python3

import argparse
import inspect
import os
import subprocess
import time
import re
from dataclasses import dataclass


def r(*path):
    """
    Takes a relative path from the directory of this python file and returns the absolute path.
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)


def run_in_dir(directory, callable):
    cwd = os.getcwd()
    os.chdir(directory)
    result = callable()
    os.chdir(cwd)
    return result


def main(compile_output_dir_path):
    def format_bytes(num_of_bytes):
        if num_of_bytes < 1024:
            return f"{num_of_bytes} B"
        elif num_of_bytes < 1024**2:
            return f"{num_of_bytes/1024:.1f} KiB"

    @dataclass
    class FunctionStackUsage:
        su_file_path: str
        file_path: str
        line_number: int
        column_number: int
        function_name: str
        stack_usage: int
        staticness: str

    all_functions = []
    for root, dirs, filenames in os.walk(compile_output_dir_path):
        for filename in filter(lambda filename: filename.endswith(".su"), filenames):
            su_file_path = os.path.join(root, filename)
            with open(su_file_path) as file:
                stack_usage_info = file.read()
            for (
                file_path,
                line_number,
                column_number,
                function_name,
                stack_usage,
                staticness,
            ) in re.findall(
                r"(.+):(\d+):(\d+):([\w\.]+)\s+(\d+)\s*(.*)\n", stack_usage_info
            ):
                all_functions.append(
                    FunctionStackUsage(
                        su_file_path,
                        file_path,
                        int(line_number),
                        int(column_number),
                        function_name,
                        int(stack_usage),
                        staticness,
                    )
                )

    print("64 functions with the highest stack usage")
    print(f'+{"-"*48}+{"-"*12}+')
    for function in sorted(all_functions, key=lambda f: f.stack_usage, reverse=True)[
        :64
    ]:
        print(
            f'|{f" {function.function_name}":<48}|{f"{format_bytes(function.stack_usage)} ":>12}|'
        )
    print(f'+{"-"*48}+{"-"*12}+')
    print()
    print(
        "NOTE: This analysis only looks at the stack usage of each function isolated and"
    )
    print(
        "does not take into account how much stack each function actually needs when calling"
    )
    print(
        "other functions. This is not even possible to determine by static analysis alone."
    )


if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser()
    for parameter in inspect.signature(main).parameters:
        argument_parser.add_argument(parameter)
    arguments = argument_parser.parse_args()
    main(**vars(arguments))
